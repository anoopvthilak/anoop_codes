//BEGIN Table responsive column count
if ($(window).width() < 775) {
    $(dom_id + " .db_data_table td:not(:nth-child(3))").css({
        "display": "none",
        "opacity": 0
    });
    var index = 3,
        time = 0;
    setInterval(function () {
        $(dom_id + " .db_data_table td").finish();
        $(dom_id + " .db_data_table td").stop(true, true);
        if (index == 3)
            index = 1;
        else
            index++;

        $(dom_id + " .db_data_table td:not(:nth-child(" + index + "))").animate({
            "opacity": 0
        }, function () {
            $(this).css("display", "none");

            $(dom_id + " .db_data_table td:nth-child(" + index + ")").css({
                "display": "table-cell"
            }).animate({
                "opacity": 1
            });
        });
        time++;
    }, 3000);
}
//**END Table responsive column count

//BEGIN Update counts
$.ajax({
    beforeSend: function (jqXHR) {
        handy.jqXHR_pool.push(jqXHR);
    },
    complete: function (jqXHR) {
        handy.jqXHR_pool_remove(jqXHR);
    },
    url: modular.variables.server_script_path + "/about_us/counts.php"
}).done(function (data) {
    try {
        data = JSON.parse(data);
    } catch (e) {
        console.log(data);
        return;
    }
    //    console.log(data);
    db_data_counter(data.Brands, data["Product types"], data.Products);
});


function db_data_counter(brand_count, types_count, products_count) {

    var dom_brands = $(dom_id + " .db_data_table tr:nth-child(2) td:nth-child(1)");
    var dom_types = $(dom_id + " .db_data_table tr:nth-child(2) td:nth-child(2)");
    var dom_products = $(dom_id + " .db_data_table tr:nth-child(2) td:nth-child(3)");

    new counter({
        dom: dom_products,
        value: products_count,
        increment: 4,
        interval: 15,
        string: "+"
    });
    //    new counter(dom_products, products_count, 15,"+");
    //    new counter(dom_types, types_count);
    dom_brands.html(brand_count);
    dom_types.html(types_count + "+");
}

function counter(obj) {
    obj.increment_rate = obj.increment_rate || 1;
    counter = 0;
    var interval = setInterval(function () {
        obj.dom.html(counter + (obj.string || ""));
        counter = counter + obj.increment;
        if (counter > obj.value) {
            obj.dom.html(obj.value + (obj.string || ""));
            clearInterval(interval);
        }
    }, obj.interval || 0);
};
//**END update counts


$.ajax({
    beforeSend: function (jqXHR) {
        handy.jqXHR_pool.push(jqXHR);
    },
    complete: function (jqXHR) {
        handy.jqXHR_pool_remove(jqXHR);
    },
    url: modular.variables.server_script_path + "/about_us/fetch_brands.php"
}).done(function (data) {
    try {
        data = JSON.parse(data);

    } catch (e) {
        console.log(data);
        return;
    }
    //    console.log(data);
    append_brands(data.Brands);
});

var brand_template = $(dom_id + " .brand_details_container").clone();
       brand_template.css("display", "inline-block");
function append_brands(brand_data) {
    for (var i = 0; i < brand_data.length; i++) {
 
        var node = brand_template.clone();

        node.find(".brand_logo").attr("src", modular.project_path + "/www/res/images/about_us/brands/" + brand_data[i].brand_id + ".png");
        node.find(".brand_name").html(brand_data[i].Name);
        node.find(".brand_caption").html(brand_data[i].Caption);
        $(dom_id + " .brands_container").append(node);
    }
}
