obj_this = this;

this.load_product = function (prod_id, show_loading = 0) {
    obj_this.prod_id = prod_id;
    if (show_loading == 1) {
        $(dom_id + " .view_product_page_container").css("opacity", "0");
        $(dom_id + " .product_info_top_container").css("display", "none");
        $(dom_id + " .loading").css("display", "block");
    }
    $.ajax({
        beforeSend: function (jqXHR) {
            handy.jqXHR_pool.push(jqXHR);
        },
        complete: function (jqXHR) {
            handy.jqXHR_pool_remove(jqXHR);
        },
        url: modular.variables.server_script_path + "/product/fetch_product.php",
        data: {
            'prod_id': prod_id
        },
        type: "POST"
    }).done(function (return_data) {
        try {
            return_data = JSON.parse(return_data);
        } catch (e) {
            console.log(return_data);
            return;
        }
        //        console.log(return_data);

        $("title").html(return_data.product_data["Brand"] + " " + return_data.product_data["Product name"] + " " + return_data.product_data["Product type"]);
        var price = return_data.product_data["Offer price"];
        if (price == "") {
            price = return_data.product_data["MRP"];
        }
        $("meta[name=description]").html("Rs " + price + " | " + return_data.product_data["Product type"] + " | " + return_data.product_data["Brand"] + " " + return_data.product_data["Product name"]);
        append_product_details(return_data);
        $(dom_id + " .view_product_page_container").css("opacity", "1");
        $(dom_id + " .product_info_top_container").css("display", "block");
        $(dom_id + " .loading").css("display", "none");

        $(dom_id + " .selected_product_image").attr({
            "src": modular.variables.content_delivery_network + "/product_images/500px/" + prod_id + "_500px.jpg"
        });

        $(dom_id + " .selected_product_name").attr({
            "value": "I want this " + return_data.product_data["Product name"]
        });

    });

}

var highlight_template = $(dom_id + " .single_highlight_container").clone();

function append_product_details(return_data) {
    $(dom_id + " .product_image").attr("src", "");
    $(dom_id + " .product_image").attr("src", modular.variables.content_delivery_network + "/product_images/500px/" + return_data.product_data['prod_id'] + "_500px.jpg");
    $(dom_id + " .product_brand").attr("src", modular.variables.content_delivery_network + "/brand_logos/" + return_data.product_data['Brand'] + ".png");
    $(dom_id + " .product_name").html(return_data.product_data["Product name"]);
    $(dom_id + " .product_type").html(return_data.product_type);
    if (return_data.product_data["Offer price"] == "") {
        $(dom_id + " .MRP," + dom_id + " .off_percentage").css("display", "none");
        $(dom_id + " .offer_price").html("Rs." + return_data.product_data["MRP"] + "/-")
    } else {
        $(dom_id + " .MRP").html("Rs." + return_data.product_data["MRP"] + "/-");
        $(dom_id + " .offer_price").html("Rs." + return_data.product_data["Offer price"] + "/-");
        var offer_percentage = ((return_data.product_data.MRP - return_data.product_data["Offer price"]) / return_data.product_data.MRP) * 100;
        offer_percentage = Math.floor(offer_percentage);
        $(dom_id + " .off_percentage").html(offer_percentage + "% Off");
    }

    if (return_data.product_data.Stock == "Out of stock") {
        $(dom_id + " .stock").css("color", "red");
    }
    $(dom_id + " .stock").html(return_data.product_data.Stock + ".");

    $(dom_id + " .highlights_container").html("");
    for (var key in return_data.product_data["Highlights"]) {
        if (return_data.product_data["Highlights"][key] != "") {

            var single_node = highlight_template.clone();
            single_node.find(".highlight_title").html(key);
            single_node.find(".highlight_value").html(return_data.product_data["Highlights"][key]);
            single_node.appendTo(dom_id + " .highlights_container");
        }
    }

    var result = !Object.values(return_data.product_data.Specifications).every(element => element === "");
    //    console.log(result);
    //    return;
    $(dom_id + " .specifications_container").css("display", "none");
    $(dom_id + " .specifications_table").html("");
    if (result) {
        $(dom_id + " .specifications_container").css("display", "block");

        for (var key in return_data.product_data.Specifications) {

            if (return_data.product_data.Specifications[key] != "") {
                var row = $("<tr />");
                $("<td />").html(key).appendTo(row);
                $("<td />").html(return_data.product_data.Specifications[key]).appendTo(row);
                row.appendTo(dom_id + " .specifications_table");
            }
        }
    }





    $(dom_id + " .other_features_container").css("display", "none");
    if ("Other features" in return_data.product_data) {
        if (return_data.product_data["Other features"] != "") {

            $(dom_id + " .other_features_container").css("display", "block");
            var row = $("<tr />");
            $("<td />").html(return_data.product_data["Other features"]).css({
                "font-weight": "400",
                "line-height": "1.5",
                "color": "#333",
                "white-space": "pre-wrap"
            }).appendTo(row);
            $(dom_id + " .other_features_table").html(row);
        }
    }
}


$(dom_id + " .send_enquiry").on("click", function () {
    //        console.log(obj_this.name);
    $(dom_id + ' .enquiry_inputs_container').css({
        'display': 'block'
    });

    $(dom_id + " .product_enquiry_popup," + dom_id + " .ghost").css("display", "block");
});

$(dom_id + " .close_button").on("click", function () {
    $(dom_id + " .product_enquiry_popup," + dom_id + " .ghost").css("display", "none");
});
var enquiry_confirmation_timer;
$(dom_id + " .product_enquiry_form").on("submit", function () {
    var form_data = handy.get_form_data(dom_id + " .product_enquiry_form");
    delete form_data.prod_name;
    form_data.prod_id = obj_this.prod_id;
    console.log(form_data);
    $.ajax({
        url: modular.variables.server_script_path + "/product/save_product_enquiry.php",
        data: {
            'form_data': JSON.stringify(form_data)
        },
        type: "POST"
    }).done(function (data) {
        //                console.log(data);
        clear_form(dom_id + " .product_enquiry_form");
        $(dom_id + " .product_enquiry_popup").css({
            "display": "none"
        });
        $(dom_id + ' .loading').css({
            'display': 'none'
        });
        $(dom_id + ' .message_box').css({
            'display': 'block'
        });
        enquiry_confirmation_timer = setTimeout(function () {
            $(dom_id + ' .message_box').css({
                'display': 'none'
            });
            $(dom_id + ' .ghost').css({
                'display': "none"
            });
        }, 5000);
    });
});
$(dom_id + ' .ok_button').on('click', function () {
    clearTimeout(enquiry_confirmation_timer);
    $(dom_id + ' .message_box').css({
        'display': 'none'
    });
    $(dom_id + ' .ghost').css({
        'display': "none"
    });

});

$(dom_id + " .product_enquiry_form textarea[name=Description]").on("keyup", function () {
    $(dom_id + " .product_enquiry_form .character_count").html($(this).val().length);
});

function clear_form(form) {
    $(form + " input").each(function () {
        if ($(this).attr("name") == "prod_name") {
            return;
        }
        $(this).val("");
    });
    $(form + " select").each(function () {
        $(this).val("");
    });
    $(form + " textarea").each(function () {
        $(this).val("");
    });
    $(form + " .character_count").html("0");
}
