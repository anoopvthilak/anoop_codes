$(dom_id + ' .enq_popup_container').on('click', function () {

    $(dom_id + ' .enquiry_form_container,' + dom_id + ' .ghost').css("display", "block").animate({
        'opacity': '1'
    });
    $(dom_id + ' .enquiry_inputs_container').css({
        'display': 'block'
    });

    $(dom_id + ' .enq_popup_container').css({
        'display': 'none'
    });

    $(dom_id + ' .enq').css({
        'display': 'none'
    });
});
$(dom_id + ' .enquiry_form_container .close_button').on('click', function (e) {
    e.preventDefault();
    clear_form(dom_id + ' .enquiry_form_container');
    $(dom_id + ' .enquiry_form_container,' + dom_id + ' .ghost').animate({
        'opacity': '0'
    }, function () {
        $(dom_id + ' .enquiry_form_container,' + dom_id + ' .ghost').css('display', 'none');
    });

    $(dom_id + ' .enq_popup_container').css({
        'display': 'block'
    });

    $(dom_id + ' .enq').css({
        'display': 'block'
    });

});

var enquiry_confirmation_timer;

$(dom_id + ' .enquiry_form_container').on('submit', function () {
    var name = $(dom_id + " .enquiry_form_container input[name=Fullname]");
    if (name.val() == "") {
        form_error_element = name;
        show_error_box("Please enter your fullname.");
        return;
    }
    var gender = $(dom_id + " .enquiry_form_container select[name=Gender]");
    if (gender.val() == "") {
        form_error_element = gender;
        show_error_box("Please select your gender.");
        return;
    }
    var email = $(dom_id + " .enquiry_form_container input[name=Email]");
    var re = /\S+@\S+\.\S+/;
    if (!(re.exec(email.val()))) {
        form_error_element = email;
        show_error_box("Please enter valid email.");
        return;
    }
    var number = $(dom_id + " .enquiry_form_container input[name='Phone number']");
    var regex = new RegExp(/^[0-9]{10}$/);
    if (!(regex.exec(number.val()))) {
        form_error_element = number;
        show_error_box("Please enter valid 10 digit phone number.");
        return;
    }
    var contact_method = $(dom_id + " .enquiry_form_container select[name='Preferred contact method']");
    if (contact_method.val() == "") {
        form_error_element = contact_method;
        show_error_box("Please select your preferred contact method.");
        return;
    }
    var language = $(dom_id + " .enquiry_form_container select[name='Preferred language']");
    if (language.val() == "") {
        form_error_element = language;
        show_error_box("Please select your preferred language.");
        return;
    }
    var description = $(dom_id + " .enquiry_form_container textarea[name=Description]");
    if (description.val().length < 20) {
        form_error_element = description;
        show_error_box("Please give a description not less than 20 characters");
        return;
    }

    function show_error_box(error_message) {
        $(dom_id + " .ghost_form").css("display", "block");
        $(dom_id + " .alert_box").css("display", "block").find(" .error_message").html(error_message);

    }
    $(dom_id + ' .enquiry_inputs_container').css({
        'display': 'none'
    });
    $(dom_id + ' .loading').css({
        'display': 'block'
    });

    var form_data = handy.get_form_data(dom_id + ' .enquiry_form_container');
    //        console.log(form_data);
    $.ajax({
        url: modular.variables.server_script_path + "/enquiry/save_enquiry.php",
        data: {
            enquiry_form_data: JSON.stringify(form_data)
        },
        type: "POST"
    }).done(function (data) {
        //        console.log(data);
        clear_form(dom_id + " .enquiry_form_container");
        $(dom_id + ' .loading').css({
            'display': 'none'
        });
        $(dom_id + ' .enquiry_form_container').css({
            'display': "none"
        });
        $(dom_id + ' .message_box').css({
            'display': 'block'
        });
        enquiry_confirmation_timer = setTimeout(function () {
            $(dom_id + ' .message_box').css({
                'display': 'none'
            });
            $(dom_id + ' .ghost').css({
                'display': "none",
                'opacity': '0'
            });
            $(dom_id + ' .enq_popup_container').css({
                'display': 'block'
            });

            $(dom_id + ' .enq').css({
                'display': 'block'
            });

        }, 5000);
    });
});
$(dom_id + ' .ok_button').on('click', function () {
    clearTimeout(enquiry_confirmation_timer);
    $(dom_id + ' .message_box').css({
        'display': 'none'
    });
    $(dom_id + ' .ghost').css({
        'display': "none",
        'opacity': '0'
    });
    $(dom_id + ' .enq_popup_container').css({
        'display': 'block'
    });

    $(dom_id + ' .enq').css({
        'display': 'block'
    });

});

$(dom_id + " .alert_box .close_alert_box").on("click", function () {
    $(dom_id + " .alert_box," + dom_id + " .ghost_form").css("display", "none");
    form_error_element.focus();
});
$(dom_id + " .enquiry_form_container textarea[name=Description]").on("keyup", function () {
    $(dom_id + " .enquiry_form_container .character_count").html($(this).val().length);
});

function clear_form(form) {
    $(form + " input").each(function () {
        $(this).val("");
    });
    $(form + " select").each(function () {
        $(this).val("");
    });
    $(form + " textarea").each(function () {
        $(this).val("");
    });
    $(form + " .character_count").html("0");
}
