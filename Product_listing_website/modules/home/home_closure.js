//BEGIN Categories_block
$.ajax({
    url: modular.variables.server_script_path + "/home/fetch_categories.php"
}).done(function (data) {
    try {
        data = JSON.parse(data);
    } catch (e) {
        console.log(data);
        return;
    }

    append_categories("domestic", data.domestic_categories);
    append_categories("commercial", data.commercial_categories);
    styler.auto_arrange(dom_id + " .categories_container", dom_id + " .category_details_container");

});


var category_template = $(dom_id + " .domestic .category_details_container").clone();
category_template.css("display", "block");

function append_categories(type, categories) {
    for (var i = 0; i < categories.length; i++) {
        var single_element = category_template.clone();
        single_element.find(".img_container").css("background-image", "url('" + modular.project_path + categories[i].image + "')");
        single_element.find(".category_type_name").html(categories[i].title);
        single_element.find(".category_type_description").html(categories[i].description);
        //    $(dom_id + " .categories_container").append(single_element);
        if (type == "domestic")
            single_element.appendTo(dom_id + " .domestic");
        else if (type == "commercial")
            single_element.appendTo(dom_id + " .commercial");
    }

}
//**END Categories_block

//BEGIN image slider
slider(dom_id + " .offer_images_slider_container", [
    modular.project_path + "/www/res/images/Home/offers/offer1.jpg",
    modular.project_path + "/www/res/images/Home/offers/offer2.jpg",
    modular.project_path + "/www/res/images/Home/offers/offer3.jpg",

]);

function slider(container, image_list) {
    var current_slide = 0,
        timeout_slide_changer;

    function initialize_timeout_slide_changer() {
        timeout_slide_changer = setTimeout(function () {
            current_slide++;
            if (current_slide == image_list.length)
                current_slide = 0;
            change_slide();
        }, 4000);
    };
    initialize_timeout_slide_changer();

    $(container + ' .image_slides_container').html("");
    for (var i = 0; i < image_list.length; i++) {
        var node = $("<div />").addClass("slide" + i + " image_slides").css("background-image", "url('" + image_list[i] + "')");
        $(container + ' .image_slides_container').append(node);
    }
    $(container + " .right_arrow").on("click", function () {
        current_slide++;
        if (current_slide == image_list.length)
            current_slide = 0;
        change_slide();
    });
    $(container + " .left_arrow").on("click", function () {
        current_slide--;
        if (current_slide == -1)
            current_slide = image_list.length - 1;
        change_slide();
    });

    function change_slide() {
        clearTimeout(timeout_slide_changer);
        $(container + ' .image_slides_container').stop(true);
        $(container + ' .image_slides_container').animate({
            scrollLeft: ($(container + ' .image_slides_container').width() * current_slide)
        }, 500, function () {
            initialize_timeout_slide_changer();
        });
    }
    $(container + " .scroll_prevent_cover").on("swiperight", function () {
        alert("clicki");
    });

}
//**END image slider

//**END Services_block

//BEGIN Services_block
//$.ajax({
//    beforeSend: function (jqXHR) {
//        handy.jqXHR_pool.push(jqXHR);
//    },
//    complete: function (jqXHR) {
//        handy.jqXHR_pool_remove(jqXHR);
//    },
//    url: modular.variables.server_script_path + "/home/services.php"
//}).done(function (response) {
//    try {
//        response = JSON.parse(response);
//    } catch (e) {
//        console.log(response);
//        return;
//    }
//    append_services(response);
//})
//
//function append_services(services_list) {
//    var single_work_template = $(dom_id + " .single_work_container").clone();
//    $(dom_id + " .work_types_container").append(single_work_template);
//    for (var i = 0; i < services_list.length; i++) {
//        console.log(services_list[i]);
//        var node = single_work_template.clone().css("display", "block").on("click", bookservice);
//        node.find(".service_image").attr("src", modular.project_path + "/www/res/images/work_icons/" + services_list[i].Image);
//        node.find(".service_name").html(services_list[i].Name);
//        $(dom_id + " .work_types_container").append(node);
//    }
//    styler.auto_arange(dom_id + " .work_types_container", dom_id + " .single_work_container", 30);
//}
//
//function bookservice() {
//    
//    modular.goto("/BookService/"+$(this).find(".service_name").html());
//}
//**END Services_block
