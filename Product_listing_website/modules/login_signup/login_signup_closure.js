//BEGIN Sign in
$(dom_id + " .sign_in_form").on("submit", function (event) {
    $(dom_id + " .sign_in_form").css("display", "none");
    $(dom_id + " .login_container .loading").css("display", "block");
    event.preventDefault();
    var phone_number = $(this).find("input[name='phone_number']").val();
    var password = $(this).find("input[name='password']").val();
    $.ajax({
        type: "POST",
        url: modular.variables.server_script_path + "/login_signup/login.php",
        data: {
            phone_number: phone_number,
            password: password
        }
    }).done(function (data) {
        try {
            data = JSON.parse(data);
        } catch (e) {
            console.log(data);
            return;
        }
        //        console.log(data);
        if (data["status"] == "Authentication successful")
            successful_login(data);
        else
            authentication_failed();
    });
});

function successful_login(data) {
    $(dom_id + " .login_container .loading").css("display", "none");
    $(dom_id + " .signed_in").css("display", "block");

    setTimeout(function () {
        $(dom_id + " .login_container").animate({
            "opacity": 0
        }, function () {
            $(dom_id + " .login_container").css("display", "none");
        });
    }, 500);

    console.log("login successful");
    window.localStorage.setItem("sid", data.sid);
    window.localStorage.setItem("pass_key", data.pass_key);
    modular.global.header.obj_js.show_user_fullname(data["Full name"]);
};

function authentication_failed() {
    $(dom_id + " .login_container .loading").css("display", "none");
    $(dom_id + " .sign_in_form").css("display", "block");
    console.log("login failed");
    $(dom_id + " .sign_in_form .wrong_credentials_message").css("display", "block");
};
//**END Sign in

//BEGIN Sign up
$(dom_id + " .sign_up_form").on("submit", function (event) {
    $(dom_id + " .sign_up_form").css("display", "none");
    $(dom_id + " .login_container .loading").css("display", "block");
    event.preventDefault();
    var phone_number = $(this).find("input[name='phone_number']").val();
    var email = $(this).find("input[name='email']").val();
    $.ajax({
        type: "POST",
        url: modular.variables.server_script_path + "/login_signup/sign_up.php",
        data: {
            phone_number: phone_number,
            email: email
        }
    }).done(function (data) {
        try {
            data = JSON.parse(data);
        } catch (e) {
            console.log(data);
            return;
        }
        console.log(data);
        if (data["status"] == "Success") {
            console.log("Sign up validation successful");
        } else {
            if (data["log"].indexOf("Email exists") != -1)
                sign_up_failed("An account already exists with email entered.");
            else if (data["log"].indexOf("Phone number exists") != -1)
                sign_up_failed("An account already exists with phone number entered.")
        }

    });
});

function sign_up_failed(message) {
    $(dom_id + " .login_container .loading").css("display", "none");
    $(dom_id + " .sign_up_form").css("display", "block");
    console.log("Sign up failed");
    $(dom_id + " .sign_up_form .credentials_exist_message").html(message);
    $(dom_id + " .sign_up_form .credentials_exist_message").css("display", "block");
};
//**END Sign up
