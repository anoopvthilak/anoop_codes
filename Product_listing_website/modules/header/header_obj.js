$(dom_id + " .ham").on("click", function () {
    modular.global.side_bar.obj_js.show_side_bar();
});


//BEGIN Search functions
var suggestions_xhr, suggestions_list = [],
    selected_suggestion_index = -1;

$(dom_id + " .search_button").on("click", function () {
    if ($(dom_id + " input[name='search_input']").val() == "") {
        return;
    }
    load_search();
});

$(dom_id + " input[name='search_input']").on("input", function () {
    $(dom_id + " .suggestions_container").css("display", "none");
    suggestions = [];
    if (typeof suggestions_xhr != "undefined")
        suggestions_xhr.abort();
    var search_string = $(dom_id + " input[name='search_input']").val();
    if (search_string == "")
        return;

    suggestions_xhr = $.ajax({
        beforeSend: function (jqXHR) {
            handy.jqXHR_pool.push(jqXHR);
        },
        complete: function (jqXHR) {
            handy.jqXHR_pool_remove(jqXHR);
        },
        url: modular.variables.server_script_path + "/header/suggestion_box_results.php",
        data: {
            'Search_string': search_string
        },
        type: "POST"
    }).done(function (return_data) {

        try {
            return_data = JSON.parse(return_data);
        } catch (e) {
            console.log(return_data);
            return;
        }
        console.log(return_data);

        selected_suggestion_index = -1;
        suggestions_list = return_data;

        if (suggestions_list.length > 0 && $(dom_id + " input[name='search_input']").is(":focus"))
            $(dom_id + " .suggestions_container").css("display", "table");

        append_suggestions();
    });

}).on("keydown", function (e) {
    if (e.keyCode == 38 || e.keyCode == 40 || e.keyCode == 13)
        e.preventDefault();
}).on("keyup", function (e) {

    if (e.keyCode == 38) {
        //Up arrow
        change_suggestion_selection("up");
    } else if (e.keyCode == 40) {
        change_suggestion_selection("down");
        //Down arrow
    } else if (e.keyCode == 13) {
        $(dom_id + " input[name='search_input']").blur();
        if (selected_suggestion_index == -1) {
            load_search();
        } else {
            view_product(suggestions_list[selected_suggestion_index].prod_id);
        }

    }
}).focusin(function () {
    selected_suggestion_index = -1;
    $(dom_id + " .suggestions_container tr").css({
        "background-color": "white",
        "transform": "none"
    }).on("mouseover", function () {
        $(this).css({
            "background-color": "#eee",
            "transform": "scale(1.01)"
        });
    }).on("mouseout", function () {
        $(this).css({
            "background-color": "white",
            "transform": "none"
        });
    });
    if (suggestions_list.length > 0)
        $(dom_id + " .suggestions_container").css("display", "table");
}).focusout(function () {
    $(dom_id + " .suggestions_container").css("display", "none");
});


function append_suggestions() {
    $(dom_id + " .suggestions_container").html("");
    for (var i = 0; i < suggestions_list.length; i++) {
        var product_page_link = "/Product/" + suggestions_list[i].prod_id;
        var row = $("<tr />");
        row.attr("id", i);
        row.on("mousedown", function () {
            var prod_id = suggestions_list[$(this).attr("id")].prod_id;
            view_product(prod_id);
        });
        //        row.attr("id", suggestions_list[i].prod_id);
        //        row.attr("id", i);
        $("<td />").append("<img src='" + modular.variables.content_delivery_network + "/product_images/100px/" + suggestions_list[i]["prod_id"] + "_100px.jpg'>").appendTo(row);
        $("<td />").html(suggestions_list[i]["Product name"]).appendTo(row);
        $("<td />").html("<i class='fas fa-rupee-sign'></i> " + suggestions_list[i]["Offer price"]).appendTo(row);
        row.appendTo(dom_id + " .suggestions_container");
    }
}

function view_product(prod_id) {
    if (modular.current_view.url == "^/Product") {
        if (prod_id == modular.global.view_product.obj_js.prod_id) {
            return;
        }
        var url = modular.project_path + "/Product/" + prod_id;
        history.pushState("", "", url);
        modular.global.view_product.obj_js.load_product(prod_id, 1);

    } else {
        modular.goto("/Product/" + prod_id);
    }
}

function change_suggestion_selection(move) {
    if (suggestions_list.length == 0)
        return;
    var previous_index = selected_suggestion_index;
    if (move == "down") {
        if (++selected_suggestion_index == suggestions_list.length) {
            selected_suggestion_index--;
            return;
        }
    } else {
        if (--selected_suggestion_index == -2) {
            selected_suggestion_index++;
            return;
        }
    }
    console.log(selected_suggestion_index);
    $(dom_id + " .suggestions_container #" + selected_suggestion_index).css({
        "background-color": "#d0d0d0",
        "transform": "scale(1.02)"
    }).on("mouseover", function () {
        $(this).css({
            "background-color": "#d0d0d0",
            "transform": "scale(1.02)"
        });
    }).on("mouseout", function () {
        $(this).css({
            "background-color": "#d0d0d0",
            "transform": "scale(1.02)"
        });
    });

    $(dom_id + " .suggestions_container #" + previous_index).css({
        "background-color": "white",
        "transform": "none"
    }).on("mouseover", function () {
        $(this).css({
            "background-color": "#eee",
            "transform": "scale(1.01)"
        });
    }).on("mouseout", function () {
        $(this).css({
            "background-color": "white",
            "transform": "none"
        });
    });

}

function load_search() {

    var search_string = $(dom_id + " input").val();
    if (modular.current_view.url == "^/Search") {
        var url = modular.project_path + "/Search/" + search_string;
        history.pushState("", "", url);
        modular.global.product_list.obj_js.product_skip = 0;
        var products_container = "#" + modular.global.product_list.dom_id + " .products_container";
        $(products_container).html("").css("display", "none");
        var view_more_container_children = "#" + modular.global.product_list.dom_id + " .view_more_container > *";
        $(view_more_container_children).css("display", "none");

        modular.global.product_list.obj_js.search_phrase = search_string;
        modular.global.product_list.obj_js.load_search();
    } else {
        modular.goto("/Search/" + search_string);
    }
}
//**END Search functions
