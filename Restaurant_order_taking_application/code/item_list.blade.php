<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script src="{{asset('js/jquery-3.3.1.min.js')}}"></script>
    <script src="{{asset('js/fontawesome-free-5.13.0-web/all.min.js')}}"></script>
    <style>
        .item_list_page_container{
            position: relative;
            width: 100%;
            min-height: 100vh;
            margin-top: 70px;
            margin-bottom: 40px;
            background-color: white;
            font-family: "Lato", sans-serif;
        }
         .page_title{
            position: relative;
            width:95%;
            margin-left: auto;
            margin-right: auto; 
            font-size: 18px;
            font-weight: 600;
        }

        .item_container{
            position: relative;
            display: grid;
            grid-template-rows: auto;
            width: 95%;
            margin-left: auto;
            margin-right: auto; 
        }

        .single_item_container{
            position: relative;
            display: grid;
            height: 120px;
            margin-top:20px;
            box-shadow: 0 0 13px rgba(0, 0, 0, 0.4);
            overflow:hidden;
            transition: height 1s;
        }


        .item_details_container{
            position: relative;
            display: grid;
            grid-template-columns: max-content auto max-content;
            width: 100%;
            height: 120px;
        }

        .item_addon_container{
            position: relative;
            /*display: none;*/
            width:100%;
            height: auto;
            /*background: #00cc00 radial-gradient(circle at 60% 100%,#009900 5%,#00cc00 94%);;*/
            
        }

        .item_remarks_container{
            position: relative;
            /*display: none;*/
            width: 100%;
            height: auto;
            /*background: #cc3d00 radial-gradient(circle at 60% 100%,#ffa436 5%,#cc3d00 94%);;*/
        }

        .item_image{
            position: relative;
            width: 150px;
            height: 100%;
            border-right: 1px solid #333;
            background-image:url("{{asset('images/item.jpeg')}}");
            background-size: cover;
            background-position: center;
            /*background-color: rgba(0,0,0,0.7);*/
        }
         @media(max-width:480px){
                .item_image{
                    width: 100px;
                }
                ./*item_details_container{
                    grid-template-columns: auto auto;
                }*/
            }
        .item_description{
            position: relative;
            margin-left: 10px;
            /*width: 400px;*/
            display: grid;
            grid-template-rows: auto auto auto;
        }

        .item_name{
            font-size: 16px;
            padding-top: 10px;
            /*font-weight: 700;*/
            color: #333;
        }
        .item_price_container{
            font-size: 20px;
        }

        .add_to_cart{
            position: relative;
            display: grid;
            margin-right: 10px;
            align-items: center;
            justify-content: center;

        }
         .add_to_cart_container{
            position: relative;
            display: inline-block;
        }
        .minus_button{
            float:left;
            height:29px;
            width: 35px;
            border: 1px solid #333;
            cursor: pointer;
            color: grey;
            background-color: lightgrey;
        }
        .item_qty{
             float:left;
            height:29px;
            width: 35px;
            border-top:1px solid #333;
            border-bottom:1px solid #333;
            text-align: center;
            font-size: 16px;
            color: #333;
        }
        .plus_button{
             float:left;
            height:29px;
            width: 35px;
            border:1px solid #333;
            cursor: pointer;
            color: grey;
            background-color: lightgrey;
        }

        .add_to_cart_button{
            background-color: #658a43;
            color: white;
            padding:7px;
            width: 105px;
            cursor: pointer;
            border:1px solid #658a43;

        }
        .add_to_cart_button:hover{
            background-color: white;
            color: #658a43;
        }

        .customize_button{
            background-color: #f96f3a;
            color: white;
            padding:7px;
            width: 105px;
            cursor: pointer;
            border:1px solid #333;

        }
       

        .title2{
            /*padding: 5px;*/
            font-size:20px;
            text-align: left;
            padding: 5px 5px;
            box-sizing: border-box;
            color: white;
        }

        .add_on_table{
            position: relative;
            width:100%;
            /*margin:0px auto 20px auto;*/
            border-collapse: collapse;
        }
        .add_on_table thead{
            border-top: 1px solid grey;
            border-bottom: 1px solid grey;
            background: #00cc00 radial-gradient(circle at 60% 100%,#009900 5%,#00cc00 94%);
            color: white;
            padding: 5px 0px;
        }
        .add_on_table th{
            font-weight: 500;
            font-size:18px; 
            padding: 5px;
            text-align: left;
        }
        .add_on_table tr{
            border-bottom: 1px solid lightgrey;
        }
        .add_on_table td{
            /*padding-left: 20px;*/
            font-size: 18px;
            padding: 5px;
        }

        .addon_value{
            transform: scale(2);
            -ms-transform: scale(2);
            -webkit-transform: scale(2);
            padding: 10px;
            background-color: white;
        }
        .item_remarks_container textarea{
            width:100%;
            height: 80px;
            font-size: 18px;
        }
        .alert_box{
            position: fixed;
            display: none;
            opacity: 0;
            z-index: 2020;
            height: 120px;
            width:95%;
            padding: 15px 10px;
            border-radius: 5px;
            background-color: #96ac3d;
            color: white;
            /*transition: all 1s;*/
        }
    </style>

</head>
<body> 
@include('header')
    <div class="item_list_page_container">
       <!--  <div class="page_title">
            <a href="index.html"><i class="fas fa-home" style="font-size:20px;"></i> Home</a> / 
            <a href="submenu.html">Burgers </a>
            / House Signatures
        </div> -->
        <div class="item_container">
            @foreach($item_list as $item)
            <div class="single_item_container">
                <div class="item_details_container">
                    <div class="item_image"></div>
                    <div class="item_description">
                        <div class="item_name" val="{{$item->id}}">{{$item->item}}</div>
                        <div class="item_price_container">Rs. <label class="item_price">{{$item->dining_price}}</label>/-</div>
                        <!-- <div class="item_details">Blanched Prawns, tomatoes, boiled egg and a tangy mayo</div> -->
                        
                    </div>
                    <div class="add_to_cart">
                        <div class="add_to_cart_container">
                            <button class="minus_button"><i class="fas fa-minus"></i></button>
                            <input class="item_qty" value="1">
                            <button class="plus_button"><i class="fas fa-plus"></i></button>
                        </div>
                        <button class="add_to_cart_button">Add to <i class="fas fa-shopping-cart"></i></button>
                        <button class="customize_button">Customize</button>
                    </div>
                </div>
                @if(count($item->addon)>0) 
                <div class="item_addon_container">
                    <form class="addon_form" style="width: 100%;">
                        <table class="add_on_table">
                            <thead>
                                <tr>
                                    <th style="padding-left:  30px;">Sl No</th>
                                    <th style="width:100px;">Add-ON</th>
                                    <th style="text-align: center;">Add/Remove</th>
                                </tr>
                            </thead>
                            <tbody><?php $c=1; ?>
                                @foreach($item->addon as $addon)
                                <tr>
                                    <td style="padding-left:  50px;width:90px;"><input type="hidden" class="addon_id" value="{{$addon->id}}">{{$c++}}</td>
                                    <td><input type="hidden" class="addon_price" value="{{$addon->sp}}"><label class="addon_name">{{$addon->modifier_name}}</label></td>
                                    <td style="text-align: center;width: 50px;"><input  type="checkbox" class="addon_value"></td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </form>
                </div>
                @endif
                <div class="item_remarks_container" style="padding-right: 15px;">
                    <div class="title2" style="background: #cc3d00 radial-gradient(circle at 60% 100%,#ffa436 5%,#cc3d00 94%);">Remarks</div>
                    <form style="position: relative;width:95%;margin: 5px auto;">
                        <textarea style="width:100%;" placeholder="Enter remarks here.."></textarea>
                        <div style="width:100%;text-align: right;margin-top: 10px;">
                            <button type="reset" style="width:60px;padding:10px;background-color: grey;color: white;border-radius: 5px;" onclick="$('.addon_form').trigger('reset');">Clear</button>
                        </div>
                    </form>
                </div>
            </div>
            @endforeach
        </div>
    </div>

    <button class="alert_box lctc">Item added to <i class="fas fa-shopping-cart"></i></button>
<script>
    jQuery.fn.heightAuto = function () {
        var elem, height, width;
        return this.each(function (i, el) {
            el = jQuery(el), elem = el.clone().css({
             "height": "auto",
            }).appendTo($(this).parent());
         //            $(this).parent()
            height = elem.css("height"),
             elem.remove();

            el.css({
             "height": height
            });
        });
    }
    $(document).ready(function(){
        $(".add_to_cart_button").on("click",function(){            
            var addons=[],id,name,price; 
            $(".single_item_container").css("height","120");
            $(".customize_button").html("Customize").css("background-color","#f96f3a");

            var elem_parent=$(this).parents(".single_item_container");
            id=elem_parent.find('.item_name').attr("val");
            name=elem_parent.find('.item_name').html();
            price=elem_parent.find('.item_price').html();
            qty=parseInt(elem_parent.find('.item_qty').val());
            if(isNaN(qty)){
                qty=1;
            }else if(qty<1){
                qty=1;
            }

            var addons_elem=elem_parent.find(".item_addon_container table tbody tr");
            var addon_flag=0;
            addons_elem.each(function(index, value){
                addons[index] = {};
                if ($(this).find('.addon_value').is(":checked")){
                    addon_flag=1;
                    addons[index]['addon_checked'] = 1;
                }
                else 
                    addons[index]['addon_checked'] = 0;
                addons[index]['addon_id'] = $(this).find('.addon_id').val();
                addons[index]['addon_name'] = $(this).find('.addon_name').html();
                addons[index]['addon_price'] = $(this).find('.addon_price').val();
               
            });
            var remarks=elem_parent.find(".item_remarks_container textarea").val();
            remarks=remarks.trim();

            var item={
                item_id : id,
                name : name,
                price : price,
                qty : qty,
                addons : addons,
                addon:addon_flag?"Yes":"No",
                remarks : remarks,
                status : "fresh order"
            };

            if(addon_flag || remarks!="")
                my_cart.push(item);
            else{
                var item_exist_flag=0;
                for (var i=0;i<my_cart.length;i++){
                    if(my_cart[i]['item_id']==id && my_cart[i]['addon']=="No" && my_cart[i]['remarks']=="" ){
                        my_cart[i]['qty']+=qty;
                        item_exist_flag=1;
                        break;
                    }
                }
                if(!item_exist_flag){
                    my_cart.push(item);
                }
            }
            console.log(my_cart);
            window.localStorage.setItem('my_cart',JSON.stringify(my_cart)); 
            var cart_position=$(".cart_container").offset();
            // console.log(cart_position);
            var position=$(this).parents(".single_item_container").offset();
            $(".alert_box").finish();
            $(".alert_box").css({
                "display" : "block",
                "opacity" : ".6",
                "font-size" : "20px"
            }).offset(position);
            $(".alert_box").animate({
                "border-radius":"200px",
                "top" : "10px",
                "left" :cart_position.left,
                "width" : "70px",
                "height" : "70px",
                "font-size": "8px",
                "opacity" : "1"
            },800,"swing",function(){
                $(".alert_box").css({
                    "display":"none",
                    "width" : "95%",
                    "height" : "120px"
                });
                $('.item_count').html(my_cart.length);
            });
            $('.single_item_container form').trigger('reset');
        });
        $(".plus_button").on("click",function(){
            var qty=parseInt($(this).siblings(".item_qty").val());
            $(this).siblings(".item_qty").val(qty+1);
        });
        $(".minus_button").on("click",function(){
            var qty=parseInt($(this).siblings(".item_qty").val());
            if(qty>1){
                $(this).siblings(".item_qty").val(qty-1);
            }
        });
        $(".customize_button").on("click",function(){
            if($(this).parents(".single_item_container").css("height")=="120px"){
                maximize_item($(this).parents(".single_item_container"));
            }
            else{
                minimize_item($(this).parents(".single_item_container"));
            }
            $('.single_item_container form').trigger('reset');
        });
        function maximize_item(dom_elem){
            $(".single_item_container").css("height","120");
            $(".customize_button").html("Customize").css("background-color","#f96f3a");
            dom_elem.heightAuto();
            dom_elem.find(".customize_button").html("Minimize").css("background-color","#d23b3b");
        }
        function minimize_item(dom_elem){
            dom_elem.css("height","120");
            dom_elem.find(".customize_button").html("Customize").css("background-color","#f96f3a");
        }
    });
</script>
</body>
</html>