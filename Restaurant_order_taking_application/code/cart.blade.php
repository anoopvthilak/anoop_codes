<html>
<head>
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <script src="{{asset('js/jquery-3.3.1.min.js')}}"></script>
    <script src="{{asset('js/fontawesome-free-5.13.0-web/all.min.js')}}"></script>
	<style>
		.cart_page_container{
			position: relative;
			width:100%;
			margin: 60px 0px;
            height: auto;
		}
        .bill_details_form{
            position: sticky;
            display: grid;
            z-index: 1;
            grid-template-columns: 50% 50%;
            top:60px;
            padding: 20px 10px;    
            color: #333;        
            background-color: #eeecec;
        }
        .customer_details_container{
            position: relative;
            height: auto;
            padding: 0px 5px;
        }
        .price_details_container{
            position: relative;
            height: auto;
            padding:0px 5px;
        }
        .line{
            position: relative;
            width: 100%;
            height: 1px;
            background-color: #96ac3d;
            margin: 7px 0px;
        }
        .grand_total_price_label{
            font-size: 20px;
        }
        .grand_total_price_container{
            font-size: 22px;
            /*margin:4px;*/
            /*font-weight: 700;*/
        }


        .contact_search_container{
            position: relative;
            height: auto;
/*            margin:4px 0px;*/
            border:1px solid #96ac3d;
            width:100%;
            max-width: 200px;
            background-color: black;
        }
        .bill_details_form input[name=contact_no]{
            position: relative;
            height: 30px;
            width:100%;
            font-size: 18px;
        }
        .suggestion_box {
            position: absolute;
            display: none;
            /*z-index: 2020;*/
            /*padding: 1px;*/
            left: 0px;
            top: 100%;
            width:100%;
            height: auto;
            border:1px solid #96ac3d;
            background-color: white;
        }
        .single_suggestion_container{
            position: relative;
            display: none;
            width: 100%;
            height: auto;
            text-align:left;
            padding: 5px 10px;
            box-sizing: border-box;
            border-bottom: 1px solid #333;
        }
        .single_suggestion_container:hover{
            background-color: #96ac3d;
            color: white;
        }
        .suggestion_name{
            font-size: 16px;
        }
        .bill_details_form select{
            height: 32px;
            width:100%;
            max-width: 200px;
            box-sizing: content-box;
            padding: 0px;
            border:1px solid #96ac3d;
            background-color: white;
        }
        .bill_details_form .customer_name{
            height: 30px;
            width:100%;
            max-width: 200px;
            box-sizing: content-box;
            padding: 0px;
            border:1px solid #96ac3d;
            font-size: 18px;
        }
        .bill_details_form .label{
            /*height: 35px;*/
            /*margin:10px;*/
        }

        .add_new_item_button,.update_customer_detail_button{
            /*justify-self: end;*/
            padding: 8px;
            width:110px;
            background-color: orangered;
            border-radius:5px;
            color: white;
            cursor: pointer;
        }
        .add_new_item_button >*{
            vertical-align: middle;

        }
        .page_title{
            position: relative; 
            padding: 0px 12px; 
            font-size: 18px;
            font-weight: 600;
        }    

		.item_container, .old_items_container{
			position: relative;
            display: grid;
            grid-template-rows: auto;
            width: 95%;
            margin-left: auto;
            margin-right: auto; 
			/*background-color: blue;*/
		}
		.single_item_container{
 	        position: relative;
            display: grid;
            grid-template-rows: max-content max-content max-content;
            height: 95px;
            margin-top:8px;
            box-shadow: 0 0 13px rgba(0, 0, 0, 0.4);
            transition: height 1s;
            padding-bottom: 8px;
            /*margin-bottom: 1px solid #333;*/
            overflow:hidden;
		}
        
        .item_details_container{
            position: relative;
            display: grid;
            grid-template-columns: max-content auto 15% 10%;
            width: 100%;
            height: 95px;
            margin-bottom: 5px;
        }

        .item_addon_container{
            position: relative;
            /*display: none;*/
            width:100%;
            height: auto;
            /*background: #00cc00 radial-gradient(circle at 60% 100%,#009950 5%,#00cc00 94%);;*/
            
        }

        .item_remarks_container{
            position: relative;
            /*display: none;*/
            width: 100%;
            height: auto;
            /*background: #cc3d00 radial-gradient(circle at 60% 100%,#ffa436 5%,#cc3d00 94%);;*/
        }
		.added_item_image{
			position: relative;
			width: 100px;
            background-image: url('{{asset('images/item.jpeg')}}');
			background-position: center;
			background-size: cover;
            margin: 5px 0px 0px 5px;
		}
       
		.added_item_details{
			position: relative;
			/*width:250px;*/
			display: grid;
			grid-template-rows: auto auto;
			padding-left: 10px;
            align-items: center;
			
		}
		.added_item_name{
			font-size: 12px;
		}

        .add_to_cart_container{
            position: relative;
            display: inline-block;
        }
        .minus_button{
            float:left;
            height:24px;
            width: 30px;
            border: 1px solid #333;
            color: grey;
            background-color: lightgrey;
            cursor: pointer;
        }
        .item_qty{
            float:left;
            height:24px;
            width: 35.5px;
            border-top:1px solid #333;
            border-bottom:1px solid #333;
            text-align: center;
            font-size: 16px;
            color:#333;
        }
        .plus_button{
             float:left;
            height:24px;
            width: 30px;
            border:1px solid #333;
            color: grey;
            background-color: lightgrey;
            cursor: pointer;
        }

        .customize_button{
            background-color: #f96f3a;
            color: white;
            padding:5px;
            width: 95px;
            cursor: pointer;
            border:1px solid #333;
        }
       
        .added_item_price_container{
        	position: relative;
        	display: grid;
        	grid-template-rows: auto;
        	align-items: center;
        }
        .delete_button_container{
        	position: relative;
        	display: grid;
        	grid-template-rows: auto;
        	align-items: center;
        }

        .delete_button{
      /*      background-color: red;*/
            color: red;
            padding:7px;
            width: 30px;
            height: 30px;
            cursor: pointer;
            font-size: 20px;
            /*border:1px solid red;*/
            border-radius: 5px;

        }
        .delete_button:hover{
            background-color: white;
            color: red;
        }


        .title2{
            /*padding: 5px;*/
            font-size:20px;
            text-align: left;
            padding: 5px 5px;
            box-sizing: border-box;
            color: white;
        }

        .add_on_table{
            position: relative;
            width:100%;
            /*margin:0px auto 20px auto;*/
            border-collapse: collapse;
        }
        .add_on_table thead{
            border-top: 1px solid grey;
            border-bottom: 1px solid grey;
            background: #00cc00 radial-gradient(circle at 60% 100%,#009950 5%,#00cc00 94%);
            color: white;
            padding: 5px 0px;
        }
        .add_on_table th{
            font-weight: 500;
            font-size:18px; 
            padding: 5px;
            text-align: left;
        }
        .add_on_table tr{
            border-bottom: 1px solid lightgrey;
        }
        .add_on_table td{
            /*padding-left: 20px;*/
            font-size: 18px;
            padding: 5px;
        }

        .addon_value{
            transform: scale(2);
            -ms-transform: scale(2);
            -webkit-transform: scale(2);
            padding: 10px;
            background-color: white;
        }
        .item_remarks_container textarea{
            width:100%;
            height: 80px;
            font-size: 18px;
        }

        .all_bill_buttons_container{
            position: relative;
            display: grid;
            grid-template-columns: max-content auto max-content max-content;
            padding: 20px 0px;
            width: 95%;
            margin-left: auto;
            margin-right: auto; box-sizing: border-box;
        }
        
        .cancel_button{
            position: relative;
            display: none;
            padding: 10px 20px;
            background-color: red;
            color: white;
            border-radius: 5px;
            cursor: pointer; 
        }
        .discard_button{
            position: relative;
            display: none;
            padding: 10px 20px;
            margin-right: 10px;
            background-color: grey;
            color: white;
            border-radius: 5px;
            cursor: pointer; 
        }
        .hold_kot_buttons_container{
            position: relative;
            display: none;
            
            
        }
       
        .hold_button{
            position: relative;
            padding: 10px 20px;
            background-color: black;
            color: white;
            border-radius: 5px;
            cursor: pointer; 
        }
        .kot_button{
            position: relative;
            padding: 10px 20px;
            border-radius: 5px;
            background-color: #001f3f;
            color: white;
            cursor: pointer;
        }

	</style>
</head>
<body>
@include('header')
	<div class="cart_page_container">
        <form class="bill_details_form" method="POST" action="{{url('order/insertKOT')}}">  
         <input type="hidden" name="bill_id" value="0">
            <input type="hidden" name="status" value="K">
            <div class="customer_details_container">
                <div style="display: grid;grid-template-columns: 50% 50%;grid-row-gap: 8px;align-items: center;">
                    <div style="font-size: 18px;">Customer Details</div>
                    <button class="update_customer_detail_button" type="button" style="opacity: 0;pointer-events: none;cursor: default;"><i class="fas fa-edit"></i> Update</button>
                    <div class="table_no_label label">Table No :</div>
                    <select class="table_no" name="table_no">
                        <option disabled="" selected="">--Select--</option>
                        @foreach($tables as $tab)
                        <option value="{{$tab->id}}">{{$tab->name}}</option>
                        @endforeach
                    </select>
                    <div class="contact_no_label label">Contact No :</div>
                    <div class="contact_search_container">
                        <input class="contact_no" name="contact_no" type="text"  pattern="[0-9]{10}" oninvalid="this.setCustomValidity('Enter 10 digit phone number')" oninput="this.setCustomValidity('')">
                        <div class="suggestion_box">
                            <div class="single_suggestion_container">
                                <label class="suggestion_name"></label><br>
                                <label class="suggestion_contact"></label>
                            </div>
                        </div>
                    </div>
                    <div class="name_label label">Customer Name :</div>
                    <input class="customer_name" name="customer_name">
                    <input type="hidden" name="cart">
                    {{ csrf_field() }}
                </div>
            </div>
            <div class="price_details_container">
               
                <div style=" display: grid;grid-template-columns: auto auto;">
                     <div style="font-size: 18px;">Payment Details</div>
                    <a href="{{url('home_tab')}}" style="justify-self:end;">
                        <button class="add_new_item_button" type="button"><i class="fas fa-concierge-bell" style="font-size: 20px;"></i><span>&nbsp;&nbsp;Add Item</span>
                        </button>
                    </a>
                    <div class="bill_no_label label">Bill No :</div>
                    <label class="bill_no"></label>
                    <div class="total_price_label">Total :</div>
                    <div class="total_price_continer">Rs <label class="total_price"></label>/-</div>
                    <div class="gst_percentage_label">GST 5% :</div>
                    <div class="gst_percentage_container">Rs <label class="gst_percentage_value"></label>/-</div>
                    <div class="line"></div><div class="line"></div>
                    <div class="grand_total_price_label">Grand Total :</div>
                    <div class="grand_total_price_container">Rs <label class="grand_total_price"></label>/-</div>
                    <div class="line"></div><div class="line"></div>
                    <div class="line"></div><div class="line"></div>
                </div>
            </div>
            <button class="form_submit_button" style="display: none;" type="submit"></button>
        </form>
       
        <div class="page_title" style="padding-left: 10px;color: #333;">
            <a> Cart (<label class="cart_count"></label>)</a> 
        </div>
		<div class="item_container">
			<div class="single_item_container" style="display: none;">
                <div class="item_details_container">
    				<div class="added_item_image"></div>
    				<div class="added_item_details">
    					<div class="added_item_name">Chilled Pawn Cocktail Burger </div>
	                    <div class="add_to_cart_container">
	                        <button class="minus_button"><i class="fas fa-minus"></i></button>
	                        <input class="item_qty" value="1">
	                        <button class="plus_button"><i class="fas fa-plus"></i></button>
	                    </div>
                     	<div class="buttons_container">
                            <button class="customize_button">Customize</button>
                        </div>
    				</div>
    				<div class="added_item_price_container">
    					<div style="display: inline-block;"> Rs <label class="added_item_price"></label>/-</div>
    				</div>
    				<div class="delete_button_container">
    					<button class="delete_button">
    						<i class="fas fa-trash"></i>
    					</button>
    				</div>
                </div>
                <div class="item_addon_container">
                    <form class="addon_form" style="width: 100%;">
                        <table class="add_on_table">
                            <thead>
                                <tr>
                                    <th style="padding-left:  10px;">Sl No</th>
                                    <th style="width:120px;">Add-ON</th>
                                    <th style="text-align: center;">Add/Remove</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td style="padding-left:  20px;width:50px;"><input type="hidden" class="addon_id" value=""></td>
                                    <td class="addon_name"></td>
                                    <td style="text-align: center;width: 50px;"><input  type="checkbox" class="addon_value"></td>
                                </tr>
                            </tbody>
                        </table>
                    </form>
                </div>
                <div class="item_remarks_container" style="padding-right: 15px;">
                    <div class="title2" style="background: #cc3d00 radial-gradient(circle at 60% 100%,#ffa436 5%,#cc3d00 94%);">Remarks</div>
                    <form style="position: relative;width:98%;margin: 5px auto;">
                        <textarea style="width:100%;" placeholder="Enter remarks here.." value="kdkdh"></textarea>
                        <div style="width:100%;text-align: right;margin-top: 10px;">
                            <button type="reset" style="width:60px;padding:10px;background-color: grey;color: white;border-radius: 5px;" onclick="$('.addon_form').trigger('reset');">Clear</button>
                        </div>
                    </form>
                </div>
			</div>

		</div>
        <div class="all_bill_buttons_container">
           <div> <button class="cancel_button">Cancel Order !</button></div>
            <div></div>
            <button class="discard_button">DISCARD HOLD</button>
            <div class="hold_kot_buttons_container">
                <button class="hold_button">HOLD</button>
                <button class="kot_button">KOT</button>
            </div>
        </div>
        <div class="old_items_container" style="opacity: .6">
            <div class="old_cart_title page_title" style="padding-left: 0px;display: none;">Previously Orders</div><br>
        </div>
</div>

<script>
jQuery.fn.heightAuto = function () {
    var elem, height, width;
    return this.each(function (i, el) {
        el = jQuery(el), elem = el.clone().css({
         "height": "auto",
        }).appendTo($(this).parent());
     //            $(this).parent()
        height = elem.css("height"),
         elem.remove();

        el.css({
         "height": height
        });
    });
}
jQuery.fn.serial = function (el) {
       var obj={};
        el = jQuery(el).serializeArray();
        for(var i =0;i<el.length;i++){
            obj[el[i]['name']]=el[i]['value'];
        }
        return obj;    
}
var added_item_elem;
// console.log(old_cart);
// console.log(my_cart);
$(document).ready(function(){
    show_hide_update_button();
    if("bill_no" in bill_details){
            $(".cancel_button").css("display","block");
            var HOLD_items_found=0;
            for(var i=0;i<received_cart.length;i++){
                if(received_cart[i]["status"]=="H"){
                    HOLD_items_found=1;
                    break;
                }
            }
            if(HOLD_items_found)
            $(".discard_button").css("display","block");
        }
    $("select[name=table_no]").on("change",function(){
        bill_details['table_id']=$(this).val();
        window.localStorage.setItem('bill_details',JSON.stringify(bill_details));
        show_hide_update_button();
        // console.log(bill_details);
    });
    var contact_no_XHR={
        'abort' : function(){}
    };
    $(".single_suggestion_container").on("mousedown",function(){
        // alert();
        // console.log($(this).find(".suggestion_name").html());
        $(".contact_no").val($(this).find(".suggestion_contact").html());
        $("input[name=customer_name]").val($(this).find(".suggestion_name").html());
        $('.suggestion_box').css('display','none');
        bill_details['contact_no']=$(this).find(".suggestion_contact").html();
        bill_details['customer_name']=$(this).find(".suggestion_name").html();
        window.localStorage.setItem('bill_details',JSON.stringify(bill_details));
    });
    var suggestion_DOM=$(".single_suggestion_container").clone('true','true').css("display","block");
    $(".contact_no").on("focusin",function(){
        $(".suggestion_box").css("display","block");
    }).on("focusout",function(){
        $(".suggestion_box").css("display","none");
    }).on("input",function(){
        contact_no=$(this).val();
        $(".suggestion_box").empty();
        contact_no_XHR.abort();
        contact_no_XHR=$.ajax({
            url:"{{url('contact_search')}}",
            data:{
                'contact_string' : contact_no
            }
        }).done(function(return_data){
            // console.log(return_data);
            for(var i=0;i<return_data.length;i++){
                node=suggestion_DOM.clone('true','true')
                node.find(".suggestion_name").html(return_data[i]['name']);
                node.find(".suggestion_contact").html(return_data[i]['contact']);
                $(".suggestion_box").append(node);
            }
        });
    }).on("change",function(){
        // console.log($(this).val());
        bill_details['contact_no']=$(this).val();
        window.localStorage.setItem('bill_details',JSON.stringify(bill_details));
        show_hide_update_button();
    });
    $("input[name=customer_name]").on("change",function(){
        bill_details['customer_name']=$(this).val();
        window.localStorage.setItem('bill_details',JSON.stringify(bill_details));
        show_hide_update_button();
    });
    $(".plus_button").on("click",function(){
        var qty=parseInt($(this).siblings(".item_qty").val());
        qty++;
        $(this).siblings(".item_qty").val(qty);
        var index = $(this).parents(".single_item_container").attr("index");
        my_cart[index]['qty']=qty;
        update_prices();
        window.localStorage.setItem('my_cart',JSON.stringify(my_cart));            
    });
    $(".minus_button").on("click",function(){
        var qty=parseInt($(this).siblings(".item_qty").val());
        if(qty>1){
            qty--;
            $(this).siblings(".item_qty").val(qty);
        }else{
            return;
        }
        var index = $(this).parents(".single_item_container").attr("index");
        my_cart[index]['qty']=qty;
        update_prices();
        window.localStorage.setItem('my_cart',JSON.stringify(my_cart));
    });
    $(".customize_button").on("click",function(){
        if($(this).parents(".single_item_container").css("height")=="95px"){
            maximize_item($(this).parents(".single_item_container"));
        }
        else{
            minimize_item($(this).parents(".single_item_container"));
        }
    });
    $(".item_details_container .item_qty").on("input",function(){
        var index = $(this).parents(".single_item_container").attr("index");
        my_cart[index]['qty']=$(this).val();
        update_prices();
        window.localStorage.setItem('my_cart',JSON.stringify(my_cart));
    });
    $(".item_addon_container").on("input",".addon_value",function(){
        var index = $(this).parents(".single_item_container").attr("index");
        var addon_id = $(this).parents("tr").find(".addon_id").val();
        var addon_DOM=$(this);
        for(var i=0;i<my_cart[index]['addons'].length;i++){
            if(my_cart[index]['addons'][i]['addon_id']==addon_id){
                if (addon_DOM.is(":checked")){
                    my_cart[index]['addons'][i]['addon_checked'] = 1;
                    break;
                }else{
                    my_cart[index]['addons'][i]['addon_checked'] = 0;
                    break;
                }
            }
        }
        update_prices();
        window.localStorage.setItem('my_cart',JSON.stringify(my_cart));
        
    });
    $(".item_remarks_container textarea").on("input",function(){
        var index = $(this).parents(".single_item_container").attr("index");
        my_cart[index]['remarks']=$(this).val();
        window.localStorage.setItem('my_cart',JSON.stringify(my_cart));
    });
    $(".item_details_container .delete_button").on("click",function(){
        // window.localStorage.setItem('my_cart',JSON.stringify(my_cart));
        var index = $(this).parents(".single_item_container").attr("index");
        $(this).parents(".single_item_container").remove();
        my_cart.splice(index,1);
        $(".single_item_container").each(function(index,value){
            $(this).attr("index",index);
        });
        var item_count=parseInt($('.item_count').html());
        $('.item_count,.cart_count').html(item_count-1);
        update_prices();
        if(!my_cart.length){
            $(".hold_kot_buttons_container").css("display","none");
            show_hide_update_button();
        }
        window.localStorage.setItem('my_cart',JSON.stringify(my_cart));
    });

    $(".kot_button").on("click", function(){
        $(".ghost").css("display","block");
        $('input[name=status]').val('K');
        $(".form_submit_button").trigger("click");
    });
    $(".hold_button").on("click", function(){
        $(".ghost").css("display","block");
        $('input[name=status]').val('H');
        $(".form_submit_button").trigger("click");
    });
    $(".cancel_button").on("click", function(){
        $(".new_order_alert_box .discard_message").html("This order will be cancelled from billing, Are you sure?");
        $(".new_order_alert_box .fresh_order_yes_button").off().on("click",function(){
            clear_cart();
            window.location.href="{{url('cancel_order')}}/"+bill_details['id'];
        });
        show_alert_box();        
    });
    $(".discard_button").on("click", function(){
        $(".new_order_alert_box .discard_message").html("Items in HOLD and cart will be removed, Are you sure?");
        $(".new_order_alert_box .fresh_order_yes_button").off().on("click",function(){
            clear_cart();
            window.location.href="{{url('delete_hold_items')}}/"+bill_details['id'];
        });
        show_alert_box();     
    });
    
    $(".bill_details_form").on("submit",function(e){
        e.preventDefault();
        $("input[name=cart]").val(JSON.stringify(my_cart));
        clear_cart();
        $(this).off().submit();
    
    });
    $(".update_customer_detail_button").on("click",function(){
        $(".ghost").css("display","block");
        $.ajax({
            url: "{{url('customer_details_update')}}",
            data: {
                "_token": "{{csrf_token()}}",
                "customer_name" : bill_details['customer_name'],
                "contact_no" : bill_details['contact_no'],
                "table_id" : bill_details['table_id'],
                "bill_id" : bill_details['id'],
            },
            type: "POST"
        }).done(function(return_data){
            bill_details['customer_id']=return_data.cust_id;
            old_bill_details=$.extend({},bill_details);
            window.localStorage.setItem('bill_details',JSON.stringify(bill_details));
            window.localStorage.setItem('old_bill_details',JSON.stringify(old_bill_details));
            show_hide_update_button();
            $(".ghost").css("display","none");
        });
    });
    added_item_elem=$(".single_item_container").clone(true,true).css("display","block");
    if(old_cart.length){
        $(".old_cart_title").css("display","block");
        append_old_cart_items();
    }
    if(my_cart.length){
        append_cart_items();
         $(".hold_kot_buttons_container").css("display","block");
    }
    append_bill_details();
    update_prices();
    $('.item_count,.cart_count').html(my_cart.length);
});
function append_bill_details(){
    $("input[name=bill_id]").val(bill_details['id']);
    $(".bill_no").html(bill_details['bill_no']);
    $("select[name=table_no]").val(bill_details['table_id']);
    $("input[name=customer_name]").val(bill_details["customer_name"]);
    $("input[name=contact_no]").val(bill_details["contact_no"]);
    // if('id' in bill_details){
    //     $(".customer_details_container input").attr("readonly","true");
    //     $(".customer_details_container select").attr("disabled","true");
    // }
}

function show_hide_update_button(){
    var show_flag=0;
    if(bill_details['table_id']!=old_bill_details['table_id'])
        show_flag=1;
    if(bill_details['customer_name']!=old_bill_details['customer_name'])
        show_flag=1;
    if(bill_details['contact_no']!=old_bill_details['contact_no'])
        show_flag=1;
    if(show_flag &&(!my_cart.length && old_cart.length)){
        $(".update_customer_detail_button").css({
            "opacity" :1,
            "pointer-events" : "auto",
            "cursor" : "pointer"
        });
    }else{
        $(".update_customer_detail_button").css({
            "opacity" :0,
            "pointer-events" : "none",
            "cursor" : "default"
        });
    }
}
function append_cart_items(){
    $(".item_container").empty();
    for(var i=0;i<my_cart.length;i++){
        var node=added_item_elem.clone(true,true);
        node.find(".add_on_table tbody").empty();
        if(!my_cart[i]['addons'].length)
            node.find(".item_addon_container").css("display","none");
        for(var j=0;j<my_cart[i]['addons'].length;j++){
            if(my_cart[i]['addons'][j]['addon_checked']){
                addon_val="checked";
            }else    
                addon_val="unchecked";
            node.find(".add_on_table tbody").append("<tr><td style='padding-left: 20px;width:30px;'>"+(j+1)+"<input type='hidden' class='addon_id' value="+my_cart[i]['addons'][j]['addon_id']+"></td><td class='addon_name'>"+my_cart[i]['addons'][j]['addon_name']+"</td><td style='text-align: center;width: 50px;''><input type='checkbox' class='addon_value' "+addon_val+"></td></tr>");
        }
        node.find(".added_item_name").html(my_cart[i]['name']);
        node.find(".item_qty").val(my_cart[i]['qty']);
        node.find(".item_remarks_container textarea").text(my_cart[i]['remarks']);
        $(".item_container").append(node.attr("index",i));
    } 
}
function append_old_cart_items(){
    // $(".item_container").empty();
    for(var i=0;i<old_cart.length;i++){
        var node=added_item_elem.clone(true,true);
        node.find(".delete_button,.minus_button,.plus_button").remove();
        node.find("textarea,input").attr("readonly",true);
        node.find(".add_on_table tbody").empty();
        if(!old_cart[i]['addons'].length)
            node.find(".item_addon_container").css("display","none");
        for(var j=0;j<old_cart[i]['addons'].length;j++){
            if(old_cart[i]['addons'][j]['addon_checked']){
                addon_val="checked";
            }else    
                addon_val="unchecked";
            node.find(".add_on_table tbody").append("<tr><td style='padding-left: 20px;width:30px;'>"+(j+1)+"<input type='hidden' class='addon_id' value="+old_cart[i]['addons'][j]['addon_id']+"></td><td class='addon_name'>"+old_cart[i]['addons'][j]['addon_name']+"</td><td style='text-align: center;width: 50px;''><input type='checkbox' class='addon_value' disabled "+addon_val+"></td></tr>");
        }
        node.find(".added_item_name").html(old_cart[i]['name']);
        node.find(".item_qty").val(old_cart[i]['qty']);
        node.find(".item_remarks_container textarea").text(old_cart[i]['remarks']);
        $(".old_items_container").append(node.attr("index",i+my_cart.length));
    } 
}

function maximize_item(dom_elem){
    $(".single_item_container").css("height","95");
    $(".customize_button").html("Customize").css("background-color","#f96f3a");
    dom_elem.heightAuto();
    dom_elem.find(".customize_button").html("Minimize").css("background-color","#d23b3b");
}
function minimize_item(dom_elem){
    dom_elem.css("height","95");
    dom_elem.find(".customize_button").html("Customize").css("background-color","#f96f3a");
}
function update_prices(){ 
    var total_cart=[];
    var items_total=0;
    total_cart=my_cart.concat(old_cart);
    // console.log(total_cart);
    for (var i=0;i<total_cart.length;i++){
        var item_qty=parseInt(total_cart[i]['qty']);
        var item_price=parseFloat(total_cart[i]['price']);
        var addon_price_total=0;
        for(j=0;j<total_cart[i]['addons'].length;j++){
            if(total_cart[i]['addons'][j]['addon_checked']==1){
                addon_price_total=addon_price_total+parseFloat(total_cart[i]['addons'][j]['addon_price']);
            }
        }
        var item_total=(item_price+addon_price_total)*item_qty;
        $(".single_item_container[index="+i+"] .added_item_price").html(parseFloat(item_total).toFixed(2));
        items_total+=item_total;
    }
    var gst_amount=items_total*0.05;
    var cart_grand_total=items_total+gst_amount;
    $(".price_details_container .total_price").html(parseFloat(items_total).toFixed(2));
    $(".price_details_container .gst_percentage_value").html(parseFloat(gst_amount).toFixed(2));
    $(".price_details_container .grand_total_price").html(parseFloat(cart_grand_total).toFixed(2));
}
function check_table(){
    if(!$("select[name=table_no]").val()){
            alert("Please select table");
            return false;
        }
        else{
            return true;
        }
 }
</script>
</body>
</html>