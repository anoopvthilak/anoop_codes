<html>
<head>
    <script src="{{asset('js/jquery-3.3.1.min.js')}}"></script>
    <script src="{{asset('js/fontawesome-free-5.13.0-web/all.min.js')}}"></script>
    <style>

    button{
        padding: 0px;
        background-color: transparent;
        border: none;
    }
    .bill_page_container{
      position: relative;
      margin: 100px auto 0px auto;
      box-shadow: 0 0 50px -15px #333;
      padding:3mm;
      box-sizing: border-box;
      width: 80mm;
      text-align: center;
      overflow: hidden;
    }
    .bill_title_container{
      position: relative;
    	padding-top:5px;
    	background-color: white;
    	text-align: center;
    }
    .bill_page_container .bill_details_container{
      position: relative;
    	width:100%;
    	background-color: white;
    	font-size: 8px;
    	padding-bottom: 10px;
    	box-sizing: border-box;
      overflow: hidden;
    }
    .items_table{
      position: relative;
    	width:100%;
    	font-size: 14px;
    	border-collapse: collapse;
      table-layout: fixed;
    }
    .items_table thead{
    	/*border-bottom: 1px solid #333;*/
    }
    .items_table .item_row{
    	border-top: 1px solid #333;
    }
    .print_button_container{
    	position: relative;
      margin: 20px auto;
      text-align: center;
      background-color: transparent;
    }
    .kot_print_button{
    	padding: 10px 25px;
    	background-color: blue;
    	color: white;
    	border-radius: 5px;
    	margin-left: 20px;
    	cursor: pointer;
    }
    .new_order_button{
    	padding: 10px 25px;
    	background-color: green;
    	color: white;
    	border-radius: 5px;
    	cursor: pointer;
    }
    
    @media print { 
      html, body {
      width: 80mm;
      } 
      body >:not(.bill_page_container){
        display: none;
      }
     .bill_page_container{
      margin-top: 5px;
      box-shadow: none;
      }
    }
    @page { size: 80mm }
  </style>

</head>

<body>
	@include('header')
    <div class="bill_page_container">
    	<div class="bill_title_container">
    		<div>@if($bill_details->table_id != NULL)Table No : {{$bill_details->table_id}}@endif</div>
    		<div>KOT #{{$bill_details->bill_no}}--{{($bill_details->packing > 0)? 'TAKE AWAY' : 'EAT IN'}}--</div>
			  <div>Customer: {{$bill_details->customer->name ?? ""}}</div>
			  <div>{{date('d-m-Y H:i:s')}}</div>
		  </div>
		  <div class="bill_details_container">
			<table class="items_table">
  				<thead>
  					<tr>
  						<th style="width:8mm;text-align: left;">Sl No</th>
  						<th style="text-align: left;">Item</th>
  						<th style="width: 8mm;">Qty</th>
  					</tr>
  				</thead>
  				<tbody>
  					@foreach ($cart_items as $item) 
  						<tr class="item_row">
  							<td>{{$loop->iteration}}</td>
  							<td>{{$item->item_name}}</td>
  							<td style="text-align: center;"> x {{$item->qty}}</td>
  						</tr>
  						@foreach($item->addons as $addon)
  						@if($addon->addon_checked)
  						<tr>
  							<td></td>
  							<td colspan="2" style="padding-left: 10px;font-size: 12px;"><i class="fas fa-dot-circle"></i> {{$addon->addon_name}}</td>
  						</tr>
  						@endif
  						@endforeach
  						@if($item->remarks != "")
  						<tr class="remarks_row"> 
                <td></td>
  							<td colspan="2" style="word-wrap: break-word;">
                  <label style="font-weight: 600;font-size: 12px;">Remarks:</label> <br>
                  * {{$item->remarks}} *
                </td>
  						</tr>
  						@endif
  					@endforeach
  				</tbody>
  			</table>
		  </div> 
      <div style="width: 100%;border-bottom: 2px dotted black;"></div>
    </div>
    <div class="print_button_container">
	    	<!-- <button class="new_order_button"><i class="fas fa-receipt"></i> New Order</button> -->
	    	<button class="kot_print_button" onclick="window.print();">Print <i class="fas fa-print"></i></button>
    	</div>
</body>
<script>
	function printKot(){
    var print_DOM = $(".bill_page_container").html();
    var body_DOM = $("body").html();
    $("body").html(print_DOM);
    window.print();
    $("body").html(body_DOM);
  }
  $(document).ready(function(){
    $(".fresh_order").css("display","none");
    $(".print_kot_status").css("display","block");

  });

</script>
</html>
