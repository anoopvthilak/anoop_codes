<html>  
<head>
    <meta charset="utf-8">
    	<meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <script src="{{asset('js/jquery-3.3.1.min.js')}}"></script>
    <!-- <script src="{{asset('js/fontawesome-free-5.13.0-web/all.min.js')}}"></script> -->
    <title>{{config('app.name')}}</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
      <!-- Bootstrap 3.3.7 -->
    <link rel="stylesheet" href="{{asset('lte/bower_components/bootstrap/dist/css/bootstrap.min.css')}}">
      <!-- Font Awesome -->
    <link rel="stylesheet" href="{{asset('lte/bower_components/font-awesome/css/font-awesome.min.css')}}">
      <!-- Ionicons -->
    <link rel="stylesheet" href="{{asset('lte/bower_components/Ionicons/css/ionicons.min.css')}}">
    <!-- DataTables -->
    <link rel="stylesheet" href="{{asset('lte/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">
      <!-- Theme style -->
    <link rel="stylesheet" href="{{asset('lte/dist/css/AdminLTE.min.css')}}">
      <!-- AdminLTE Skins. Choose a skin from the css/skins
           folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="{{asset('lte/dist/css/skins/_all-skins.min.css')}}">
      <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
      <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
      <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
      <![endif]-->

      <!-- Google Font -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
	<style>

		 body{
        margin: 0px;
        font-size: 14px;
        overflow-y: scroll;
		}
		button{
		    padding: 0px;
		    background-color: transparent;
		    border: none;
		}
		a{ 
		    color: inherit;
		    text-decoration: none;
		}
		input{
		    border: 0px;
		}
		.cart_page_container{
			position: relative;
			width:100%;
             font-family: "Lato",sans-serif;
            height: auto;
		}
        
        .price_details_container{
            position: relative;
            width: 95%;
            margin-left: auto;
            margin-right: auto;
            height: auto;
            padding:0px 5px;
        }
        .line{
            position: relative;
            width: 100%;
            height: 1px;
            background-color: #96ac3d;
            margin: 4px 0px;
        }
        .grand_total_price_label{
            font-size: 20px;
        }
        .grand_total_price_container{
            font-size: 22px;
            /*margin:4px;*/
            /*font-weight: 700;*/
        }
        .page_title{
            position: relative; 
            font-size: 18px;
            font-weight: 600;
        }    

		.item_container, .old_items_container{
			position: relative;
            display: grid;
            grid-template-rows: auto;
            width: 95%;
            margin-left: auto;
            margin-right: auto; 
			/*background-color: blue;*/
		}
		.single_item_container{
 	        position: relative;
            display: grid;
            grid-template-rows: max-content max-content max-content;
            height: 104px;
            margin-top:8px;
            box-shadow: 0 0 13px rgba(0, 0, 0, 0.4);
            transition: height 1s;
            padding-bottom: 8px;
            /*margin-bottom: 1px solid #333;*/
            overflow:hidden;
		}
        
        .item_details_container{
            position: relative;
            display: grid;
            grid-template-columns: max-content auto 15% 10%;
            width: 100%;
            height: 95px;
            margin-bottom: 5px;
        }

        .item_addon_container{
            position: relative;
            /*display: none;*/
            width:100%;
            height: auto;
            /*background: #00cc00 radial-gradient(circle at 60% 100%,#009950 5%,#00cc00 94%);;*/
            
        }

        .item_remarks_container{
            position: relative;
            /*display: none;*/
            width: 100%;
            height: auto;
            /*background: #cc3d00 radial-gradient(circle at 60% 100%,#ffa436 5%,#cc3d00 94%);;*/
        }
		.added_item_image{
			position: relative;
			width: 100px;
            background-image: url('{{asset('images/item.jpeg')}}');
			background-position: center;
			background-size: cover;
            margin: 5px 0px 0px 5px;
		}
       
		.added_item_details{
			position: relative;
			/*width:250px;*/
			display: grid;
			grid-template-rows: auto auto;
			padding-left: 10px;
            align-items: center;
			
		}
		.added_item_name{
			font-size: 14px;
		}

        .add_to_cart_container{
            position: relative;
            display: inline-block;
        }
        .minus_button{
            float:left;
            height:30px;
            width: 40px;
            border: 1px solid #333;
            color: grey;
            background-color: lightgrey;
            cursor: pointer;
        }
        .item_qty{
            float:left;
            height:30px;
            width: 55px;
            border-top:1px solid #333;
            border-bottom:1px solid #333;
            border-right:1px solid #333;
            text-align: center;
            font-size: 16px;
            color:#333;
        }
        .plus_button{
             float:left;
            height:24px;
            width: 30px;
            border:1px solid #333;
            color: grey;
            background-color: lightgrey;
            cursor: 
        }

        .customize_button{
            background-color: #f96f3a;
            color: white;
            padding:5px;
            width: 95px;
            cursor: pointer;
            border:1px solid #333;
        }
       
        .added_item_price_container{
        	position: relative;
        	display: grid;
        	grid-template-rows: auto;
        	align-items: center;
        }
        .delete_button_container{
        	position: relative;
        	display: grid;
        	grid-template-rows: auto;
        	align-items: center;
        }

        .delete_button{
      /*      background-color: red;*/
            color: red;
            padding:7px;
            width: 30px;
            height: 30px;
            cursor: pointer;
            font-size: 20px;
            /*border:1px solid red;*/
            border-radius: 5px;

        }
        .delete_button:hover{
            background-color: white;
            color: red;
        }

        .title2{
            /*padding: 5px;*/
            width: 100%;
            font-size:20px;
            text-align: left;
            padding: 5px 5px;
            box-sizing: border-box;
            color: white;
        }

        .add_on_table{
            position: relative;
            width:100%;
            /*margin:0px auto 20px auto;*/
            border-collapse: collapse;
        }
        .add_on_table thead{
            border-top: 1px solid grey;
            border-bottom: 1px solid grey;
            background: #00cc00 radial-gradient(circle at 60% 100%,#009950 5%,#00cc00 94%);
            color: white;
            padding: 5px 0px;
        }
        .add_on_table th{
            font-weight: 500;
            font-size:18px; 
            padding: 5px;
            text-align: left;
        }
        .add_on_table tr{
            border-bottom: 1px solid lightgrey;
        }
        .add_on_table td{
            /*padding-left: 20px;*/
            font-size: 18px;
            padding: 5px;
        }

        .addon_value{
            transform: scale(2);
            -ms-transform: scale(2);
            -webkit-transform: scale(2);
            padding: 10px;
            background-color: white;
        }
        .item_remarks_container textarea{
            width:100%;
            height: 80px;
            font-size: 18px;
        }

        .all_bill_buttons_container{
            position: relative;
            display: grid;
            grid-template-columns: max-content auto max-content max-content;
            padding: 20px 0px;
            width: 95%;
            margin-left: auto;
            margin-right: auto; box-sizing: border-box;
        }
        
        .cancel_button{
            position: relative;
            display: none;
            padding: 10px 20px;
            background-color: red;
            color: white;
            border-radius: 5px;
            cursor: pointer; 
        }
        .discard_button{
            position: relative;
            display: none;
            padding: 10px 20px;
            margin-right: 10px;
            background-color: grey;
            color: white;
            border-radius: 5px;
            cursor: pointer; 
        }
        .hold_kot_buttons_container{
            position: relative;
            display: none;
            
            
        }
       
        .hold_button{
            position: relative;
            padding: 10px 20px;
            background-color: black;
            color: white;
            border-radius: 5px;
            cursor: pointer; 
        }
        .save_button{
            position: relative;
            padding: 10px 20px;
            border-radius: 5px;
            background-color: green;
            border:1px solid green;
            color: white;
            cursor: pointer;
        }
        .save_button:hover{
            background-color: white;
            color: green;
        }

	</style>
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  @include('layouts.header')
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    @include('layouts.sidebar')
  </aside>
 <div class="content-wrapper" style="background-color: white;">

	<div class="cart_page_container">
        <div class="price_details_container">
               
            <div style="width: 100%; display: grid;grid-template-columns: auto auto;">
                <div class="line"></div><div class="line"></div>
                <div class="line"></div><div class="line"></div>
                <div style="width: 100%; display: grid;grid-template-columns: 50% 50%;">
                    <div>Bill No : </div><div>{{$bill_no}}</div>
                    <div class="grand_total_price_label">Grand Total :</div>
                    <div class="grand_total_price_container">Rs <label class="total_price"></label>/-</div>
                </div><div></div>
                <div class="line"></div><div class="line"></div>
                <div class="line"></div><div class="line"></div>
            </div>
            <div class="page_title" style="color: #333;">
                <a> Items (<label class="cart_count"></label>)</a> 
            </div>
        </div>
       
        
		<div class="item_container">
			<div class="single_item_container" style="display: none;">
                <div class="item_details_container">
    				<div class="added_item_image"></div>
    				<div class="added_item_details">
    					<div class="added_item_name">Chilled Pawn Cocktail Burger </div>
	                    <div class="add_to_cart_container">
	                        <button class="minus_button"><i class="fa fa-minus"></i></button>
	                        <input class="item_qty" value="1">
	                        <!-- <button class="plus_button"><i class="fa fa-plus"></i></button> -->
	                    </div>
                     	<div class="buttons_container">
                            <button class="customize_button">Customize</button>
                        </div>
    				</div>
    				<div class="added_item_price_container">
    					<div style="display: inline-block;"> Rs <label class="added_item_price"></label>/-</div>
    				</div>
    				<div class="delete_button_container">
    					<button class="delete_button">
    						<i class="fa fa-trash"></i>
    					</button>
    				</div>
                </div>
                <div class="item_addon_container">
                    <form class="addon_form" style="width: 100%;">
                        <table class="add_on_table">
                            <thead>
                                <tr>
                                    <th style="padding-left:  10px;">Sl No</th>
                                    <th style="width:120px;">Add-ON</th>
                                    <th style="text-align: center;">Add/Remove</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td style="padding-left:  20px;width:50px;"><input type="hidden" class="addon_id" value=""></td>
                                    <td class="addon_name"></td>
                                    <td style="text-align: center;width: 50px;"><input  type="checkbox" class="addon_value"></td>
                                </tr>
                            </tbody>
                        </table>
                    </form>
                </div>
                <div class="item_remarks_container" style="padding-right: 0px;">
                    <div class="title2" style="background: #cc3d00 radial-gradient(circle at 60% 100%,#ffa436 5%,#cc3d00 94%);">Remarks</div>
                    <form style="position: relative;width:98%;margin: 5px auto;">
                        <textarea style="width:100%;" placeholder="Enter remarks here.." ></textarea>
                        <div style="width:100%;text-align: right;margin-top: 10px;">
                            <button type="reset" class="clear_button" style="width:60px;padding:10px;background-color: grey;color: white;border-radius: 5px;">Clear</button>
                        </div>
                    </form>
                </div>
			</div>
		</div>

        <div class="all_bill_buttons_container">
            <div></div>
            <div></div>
            <button class="discard_button">DISCARD HOLD</button>
            <div class="hold_kot_buttons_container">
                <!-- <a href="{{url('todays_order/index')}}"><button type="button" class="hold_button">Cancel</button></a> -->
                <a data-toggle="modal" data-target="#modal_cancel_bill" data-bill_id=""><button class="save_button" >Save</button></a>
            </div>
        </div>
	</div>
</div>
</div>
<div class="modal fade" id="modal_cancel_bill">
          <div class="modal-dialog">
            <form class="update_bill modal-content" action="{{url('update_bill')}}" method="POST">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Reason for Editing</h4>
              </div>
              <div class="modal-body">
                <div class="form-group">
                   <div class="col-sm-12">
                        <textarea name="reason_for_edit" id="reason_for_edit" required class="form-control"></textarea>
                        <input type="hidden" class="cart" name="cart">
                        <input type="hidden" class="bill_id" name="bill_id">
                   </div>    
                </div><br><br>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-danger pull-right update_bill_button">Update</button>
              </div>
            </form>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
    </div>
<script>
jQuery.fn.heightAuto = function () {
    var elem, height, width;
    return this.each(function (i, el) {
        el = jQuery(el), elem = el.clone().css({
         "height": "auto",
        }).appendTo($(this).parent());
     //            $(this).parent()
        height = elem.css("height"),
         elem.remove();

        el.css({
         "height": height
        });
    });
}
jQuery.fn.serial = function (el) {
       var obj={};
        el = jQuery(el).serializeArray();
        for(var i =0;i<el.length;i++){
            obj[el[i]['name']]=el[i]['value'];
        }
        return obj;    
}
var added_item_elem;

var bill_details_id,received_cart=[],my_cart=[],old_cart=[],old_bill_details={};
if(window.localStorage.getItem('my_cart')){
    my_cart=JSON.parse(window.localStorage.getItem('my_cart'));
}else 
     console.log("Fresh cart empty"); 
if(window.localStorage.getItem('bill_details_id')){
    bill_details_id=JSON.parse(window.localStorage.getItem('bill_details'));
}
if(window.localStorage.getItem('received_cart')){
    old_cart=JSON.parse(window.localStorage.getItem('old_cart'));
}
if(window.localStorage.getItem('old_cart')){
    old_cart=JSON.parse(window.localStorage.getItem('old_cart'));
}
if(window.localStorage.getItem('old_bill_details')){
    old_bill_details=JSON.parse(window.localStorage.getItem('old_bill_details'));
}
@if(isset($bill_items))
received_cart = @json($bill_items);
bill_details_id={{$bill_id}}

my_cart = @json($bill_items);
old_my_cart = @json($bill_items);
console.log("received cart");
console.log(JSON.parse(JSON.stringify(received_cart)));
@endif

$(document).ready(function() {


    console.log("mycart");
    console.log(JSON.parse(JSON.stringify(my_cart)));
    window.localStorage.setItem('my_cart',JSON.stringify(my_cart));
    window.localStorage.setItem('bill_details_id',JSON.stringify(bill_details_id));


   
    $(".new_order_alert_box .fresh_order_cancel_button").on("click",function(){
        $(".new_order_alert_box,.ghost").css("display","none");
    });

    $(".logout_button").on("click",function(e){
        e.preventDefault();
        clear_cart();
        document.getElementById('logout-form').submit();
    });
});

function clear_cart(){
    window.localStorage.removeItem("my_cart");
    window.localStorage.removeItem("old_cart");
    window.localStorage.removeItem("bill_details_id");
    window.localStorage.removeItem("old_bill_details");
}
function show(){

    if(window.localStorage.getItem("my_cart")){
        console.log("my_cart");
        console.log(JSON.parse(window.localStorage.getItem("my_cart")));
    }else
        console.log("my cart data not found");

    if(window.localStorage.getItem("old_cart")){
        console.log("old_cart");
        console.log(JSON.parse(window.localStorage.getItem("old_cart")));
    }else
        console.log("old cart data not found");

    if(window.localStorage.getItem("bill_details")){
        console.log("bill_details");
        console.log(JSON.parse(window.localStorage.getItem("bill_details")));
    }else
        console.log("bill_details data not found");
}

function show_alert_box(){
    var DOM_bill_details=$(".new_order_alert_box .bill_details_container");
    DOM_bill_details.empty();
    if("bill_no" in bill_details){
        bill_details.bill_no?DOM_bill_details.append("<div>Bill no : #"+bill_details.bill_no+"</div>"):"";

    }else{
        DOM_bill_details.append("<div>Order : New</div>");
    }
    bill_details.table_id?DOM_bill_details.append("<div>Table : "+bill_details.table_id+"</div>"):"";
    bill_details.customer_name?DOM_bill_details.append("<div>Customer name : "+bill_details.customer_name+"</div>"):"";
    bill_details.contact_no?DOM_bill_details.append("<div>Contact No : "+bill_details.contact_no+"</div>"):"";
    $(".new_order_alert_box,.ghost").css("display","block");
}


// console.log(old_cart);
// console.log(my_cart);
$(document).ready(function(){
   
    // $(".plus_button").on("click",function(){
    //     var qty=parseInt($(this).siblings(".item_qty").val());
    //     qty++;
    //     $(this).siblings(".item_qty").val(qty);
    //     var index = $(this).parents(".single_item_container").attr("index");
    //     my_cart[index]['qty']=qty;
    //     update_prices();
    //     window.localStorage.setItem('my_cart',JSON.stringify(my_cart));            
    // });
    $(".minus_button").on("click",function(){
        var qty=parseInt($(this).siblings(".item_qty").val());
        if(qty>1){
            qty--;
            $(this).siblings(".item_qty").val(qty);
        }else{
            return;
        }
        var index = $(this).parents(".single_item_container").attr("index");
        my_cart[index]['qty']=qty;
        update_prices();
        window.localStorage.setItem('my_cart',JSON.stringify(my_cart));
    });
    $(".customize_button").on("click",function(){
        if($(this).parents(".single_item_container").css("height")=="104px"){
            maximize_item($(this).parents(".single_item_container"));
        }
        else{
            minimize_item($(this).parents(".single_item_container"));
        }
    });
    $(".item_details_container .item_qty").on("change",function(){
        var index = $(this).parents(".single_item_container").attr("index");
        var old_qty= old_my_cart[index]['qty'];
        if($(this).val()<=old_qty && $(this).val()>0)
            my_cart[index]['qty']=$(this).val();
        else{
            $(this).val(old_qty);
            my_cart[index]['qty']=$(this).val();
            alert("You cannot increase the qty greater than "+old_qty);
        }
        update_prices();
        window.localStorage.setItem('my_cart',JSON.stringify(my_cart));
    });
    $(".item_addon_container").on("input",".addon_value",function(){
        var index = $(this).parents(".single_item_container").attr("index");
        var addon_id = $(this).parents("tr").find(".addon_id").val();
        var addon_DOM=$(this);
        for(var i=0;i<my_cart[index]['addons'].length;i++){
            if(my_cart[index]['addons'][i]['addon_id']==addon_id){
                if (addon_DOM.is(":checked")){
                    my_cart[index]['addons'][i]['addon_checked'] = 1;
                    break;
                }else{
                    my_cart[index]['addons'][i]['addon_checked'] = 0;
                    break;
                }
            }
        }
        update_prices();
        window.localStorage.setItem('my_cart',JSON.stringify(my_cart));
        
    });
    $(".item_remarks_container textarea").on("input",function(){
        var index = $(this).parents(".single_item_container").attr("index");
        my_cart[index]['remarks']=$(this).val();
        window.localStorage.setItem('my_cart',JSON.stringify(my_cart));
    });
    $(".item_details_container .delete_button").on("click",function(){
        // window.localStorage.setItem('my_cart',JSON.stringify(my_cart));
        var index = $(this).parents(".single_item_container").attr("index");
        if(confirm("Delete this item '"+my_cart[index]['name']+"' ?")){
            $(this).parents(".single_item_container").remove();
            my_cart.splice(index,1);
            $(".single_item_container").each(function(index,value){
                $(this).attr("index",index);
            });
            var item_count=parseInt($('.cart_count').html());
            $('.item_count,.cart_count').html(item_count-1);
            update_prices();
            window.localStorage.setItem('my_cart',JSON.stringify(my_cart));
        }
    });
    $(".clear_button").on('click',function(){
        $(this).parents(".single_item_container").find('.addon_form input[type=checkbox]').prop("checked", false);
        var index = $(this).parents(".single_item_container").attr("index");
        console.log(my_cart[index]['remarks']);
        my_cart[index]['remarks'] = "";
        console.log(my_cart[index]['remarks']);
        for(var i=0;i<my_cart[index]['addons'].length;i++){
            my_cart[index]['addons'][i]['addon_checked'] = 0;   
        }
        $(this).parents(".single_item_container").find(".item_remarks_container textarea").html("");
        update_prices();
    });
    $(".save_button").on("click", function(){
        $('input[name=cart]').val(JSON.stringify(my_cart));
        $('input[name=bill_id]').val(JSON.stringify(bill_details_id));

    });
    $(".update_bill_button").on("click",function(){
        clear_cart();
        var reason=$('#reason_for_edit').val();
        reason = reason.trim();
        // alert("-"+reason+"-");
        if (reason == "") {
            alert("Please type edited reason");
            return;
        }else{
            $(".update_bill").submit();
        }
    });
    
    // $(".hold_button").on("click", function(){
    //     $(".ghost").css("display","block");
    //     $('input[name=status]').val('H');
    //     $(".form_submit_button").trigger("click");
    // });
    // $(".cancel_button").on("click", function(){
    //     $(".new_order_alert_box .discard_message").html("This order will be cancelled from billing, Are you sure?");
    //     $(".new_order_alert_box .fresh_order_yes_button").off().on("click",function(){
    //         clear_cart();
    //         window.location.href="{{url('cancel_order')}}/"+bill_details['id'];
    //     });
    //     show_alert_box();        
    // });
    // $(".discard_button").on("click", function(){
    //     $(".new_order_alert_box .discard_message").html("Items in HOLD and cart will be removed, Are you sure?");
    //     $(".new_order_alert_box .fresh_order_yes_button").off().on("click",function(){
    //         clear_cart();
    //         window.location.href="{{url('delete_hold_items')}}/"+bill_details['id'];
    //     });
    //     show_alert_box();     
    // });
    
    
    // $(".update_customer_detail_button").on("click",function(){
    //     $(".ghost").css("display","block");
    //     $.ajax({
    //         url: "{{url('customer_details_update')}}",
    //         data: {
    //             "_token": "{{csrf_token()}}",
    //             "customer_name" : bill_details['customer_name'],
    //             "contact_no" : bill_details['contact_no'],
    //             "table_id" : bill_details['table_id'],
    //             "bill_id" : bill_details['id'],
    //         },
    //         type: "POST"
    //     }).done(function(return_data){
    //         bill_details['customer_id']=return_data.cust_id;
    //         old_bill_details=$.extend({},bill_details);
    //         window.localStorage.setItem('bill_details',JSON.stringify(bill_details));
    //         window.localStorage.setItem('old_bill_details',JSON.stringify(old_bill_details));
            
    //         $(".ghost").css("display","none");
    //     });
    // });
    added_item_elem=$(".single_item_container").clone(true,true).css("display","block");
    
    if(my_cart.length){
        append_cart_items();
         $(".hold_kot_buttons_container").css("display","block");
    }
    
    update_prices();
    $('.item_count,.cart_count').html(my_cart.length);
});

function append_cart_items(){
    $(".item_container").empty();
    for(var i=0;i<my_cart.length;i++){
        var node=added_item_elem.clone(true,true);
        node.find(".add_on_table tbody").empty();
        if(!my_cart[i]['addons'].length)
            node.find(".item_addon_container").css("display","none");
        for(var j=0;j<my_cart[i]['addons'].length;j++){
            if(my_cart[i]['addons'][j]['addon_checked']){
                addon_val="checked";
            }else    
                addon_val="unchecked";
            node.find(".add_on_table tbody").append("<tr><td style='padding-left: 20px;width:30px;'>"+(j+1)+"<input type='hidden' class='addon_id' value="+my_cart[i]['addons'][j]['addon_id']+"></td><td class='addon_name'>"+my_cart[i]['addons'][j]['addon_name']+"</td><td style='text-align: center;width: 50px;''><input type='checkbox' class='addon_value' "+addon_val+"></td></tr>");
        }
        node.find(".added_item_name").html(my_cart[i]['name']);
        node.find(".item_qty").val(my_cart[i]['qty']);
        node.find(".item_remarks_container textarea").text(my_cart[i]['remarks']);
        $(".item_container").append(node.attr("index",i));
    } 
}
// function append_old_cart_items(){
//     // $(".item_container").empty();
//     for(var i=0;i<old_cart.length;i++){
//         var node=added_item_elem.clone(true,true);
//         node.find(".delete_button,.minus_button,.plus_button").remove();
//         node.find("textarea,input").attr("readonly",true);
//         node.find(".add_on_table tbody").empty();
//         if(!old_cart[i]['addons'].length)
//             node.find(".item_addon_container").css("display","none");
//         for(var j=0;j<old_cart[i]['addons'].length;j++){
//             if(old_cart[i]['addons'][j]['addon_checked']){
//                 addon_val="checked";
//             }else    
//                 addon_val="unchecked";
//             node.find(".add_on_table tbody").append("<tr><td style='padding-left: 20px;width:30px;'>"+(j+1)+"<input type='hidden' class='addon_id' value="+old_cart[i]['addons'][j]['addon_id']+"></td><td class='addon_name'>"+old_cart[i]['addons'][j]['addon_name']+"</td><td style='text-align: center;width: 50px;''><input type='checkbox' class='addon_value' disabled "+addon_val+"></td></tr>");
//         }
//         node.find(".added_item_name").html(old_cart[i]['name']);
//         node.find(".item_qty").val(old_cart[i]['qty']);
//         node.find(".item_remarks_container textarea").text(old_cart[i]['remarks']);
//         $(".old_items_container").append(node.attr("index",i+my_cart.length));
//     } 
// }

function maximize_item(dom_elem){
    $(".single_item_container").css("height","104");
    $(".customize_button").html("Customize").css("background-color","#f96f3a");
    dom_elem.heightAuto();
    dom_elem.find(".customize_button").html("Minimize").css("background-color","#d23b3b");
}
function minimize_item(dom_elem){
    dom_elem.css("height","104");
    dom_elem.find(".customize_button").html("Customize").css("background-color","#f96f3a");
}
function update_prices(){ 
    var total_cart=[];
    var items_total=0;
    total_cart=my_cart;
    for (var i=0;i<total_cart.length;i++){
        var item_qty=parseInt(total_cart[i]['qty']);
        var item_price=parseFloat(total_cart[i]['price']);
        var addon_price_total=0;
        for(j=0;j<total_cart[i]['addons'].length;j++){
            if(total_cart[i]['addons'][j]['addon_checked']==1){
                addon_price_total=addon_price_total+parseFloat(total_cart[i]['addons'][j]['addon_price']);
            }
        }
        var item_total=(item_price+addon_price_total)*item_qty;
        $(".single_item_container[index="+i+"] .added_item_price").html(parseFloat(item_total).toFixed(2));
        items_total+=item_total;
    }
    var gst_amount=items_total*0.05;
    var cart_grand_total=items_total+gst_amount;
    $(".price_details_container .total_price").html(parseFloat(items_total).toFixed(2));
    $(".price_details_container .gst_percentage_value").html(parseFloat(gst_amount).toFixed(2));
    $(".price_details_container .grand_total_price").html(parseFloat(cart_grand_total).toFixed(2));
}

</script>
<!-- <script src="{{asset('lte/bower_components/jquery/dist/jquery.min.js')}}"></script> -->
<!-- jQuery UI 1.11.4 -->
<script src="{{asset('lte/bower_components/jquery-ui/jquery-ui.min.js')}}"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- jQuery 3 -->
<!-- <script src="{{asset('lte/bower_components/jquery/dist/jquery.min.js')}}"></script> -->
<!-- Bootstrap 3.3.7 -->
<script src="{{asset('lte/bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>
<!-- DataTables -->
<script src="{{asset('lte/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('lte/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
<!-- SlimScroll -->
<script src="{{asset('lte/bower_components/jquery-slimscroll/jquery.slimscroll.min.js')}}"></script>
<!-- FastClick -->
<script src="{{asset('lte/bower_components/fastclick/lib/fastclick.js')}}"></script>
<!-- AdminLTE App -->
<script src="{{asset('lte/dist/js/adminlte.min.js')}}"></script>
<!-- AdminLTE for demo purposes -->
<script src="{{asset('lte/dist/js/demo.js')}}"></script>
</body>
</html>