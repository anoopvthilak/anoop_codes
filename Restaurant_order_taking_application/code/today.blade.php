<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script src="{{asset('js/jquery-3.3.1.min.js')}}"></script>
    <script src="{{asset('js/fontawesome-free-5.13.0-web/all.min.js')}}"></script>
    <style>
        .todays_order_page_container{
            position: relative;
            width: 100%;
            min-height: 100vh;
            margin-top: 70px;
            margin-bottom: 40px;
            background-color: white;
            font-family: "Lato", sans-serif;
        }
         .page_title{
            position: relative;
            width:95%;
            margin-left: auto;
            margin-right: auto; 
            font-size: 18px;
            font-weight: 500;
        }
          table {
            font-family: arial, sans-serif;
            border-collapse: collapse;
            width:95%;
            margin-left: auto;
            margin-right: auto;
            border-radius: 7px;
            overflow:hidden; 
        }
        thead{
            position: sticky;
            top: 60px;
        }

        td,
        th {
            border: 1px solid #dddddd;
            text-align: left;
            padding: 8px;
            font-size: 14px;
        }

        th {
            background-color: #333;
            color: white;
        }

        tr:nth-child(even) {
            background-color: #dddddd;
        }

        .table_title_desc_container {
            position: relative;
            width: 100%;
            height: auto;
            overflow: hidden;
            padding: 50px 50px;
            box-sizing: border-box;

        }

        .table_title_desc_container .title {
            font-size: 30px;
            color: blue;
            font-family: sans-serif;
        }


    </style>

</head>
<body> 
@include('header')
    <div class="todays_order_page_container">
        <div class="page_title">
            <a><i class="fas fa-calendar" style="font-size:20px;color:orangered;"></i> Today's orders</a> 
        </div>
        <table class="orders_table">
            <thead>
              <tr>
                <th>BILL NO</th>
                <th>CUSTOMER</th>
                <th>TABLE NO</th>
                <th>AMOUNT</th>
                <th>STATUS</th>
                <th style="text-align: center;">EDIT</th>
              </tr>
            </thead>
            <tbody>
                @foreach($bills as $bill)
                <tr>
                   <td>{{$bill->bill_no}}</td>
                   <td>{{$bill->customer_id ? ($bill->customer->name ?? $bill->customer->contact): ""}}</td>
                   <td>{{$bill->table->name ?? ''}}</td>
                   <td>{{$bill->total}}</td>
                   <td>@if($bill->status == 'G') 
                            Generated 
                        @elseif($bill->status == 'C') 
                            Cancelled 
                        @elseif($bill->status == 'F') 
                            FOC 
                        @else 
                            Hold 
                        @endif
                    </td>
                    <td style="text-align: center;color: #20B2AA;">
                        @if($bill->status == 'K' || $bill->status == 'H') 
                            <a href="{{route('order.edit',$bill->id)}}" class="edit_order_button"><i class="fas fa-edit"></i></a>
                        @endif
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>

    </div>

<div class="table_title_desc_container">
    <!-- <div class="title">dsfkhdsfh</div>< -->
</div>
<script>
    $(document).ready(function() {
        $(".edit_order_button").on("click",function(e){
            e.preventDefault();
            var href=$(this).attr("href");
            if(is_order_fresh()){
            window.location.href=href;
            return;
            }
            $(".new_order_alert_box .fresh_order_yes_button").off().on("click",function(){
                clear_cart();
                window.location.href=href;
            });
            show_alert_box();
        });
    });
</script>
</body>
</html>
