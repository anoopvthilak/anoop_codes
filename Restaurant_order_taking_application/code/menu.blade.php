<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script src="{{asset('js/jquery-3.3.1.min.js')}}"></script>
    <script src="{{asset('js/fontawesome-free-5.13.0-web/all.min.js')}}"></script>
    <style>
        .page_container{
            position: relative;
            width: 100%;
            min-height: 100vh;
            margin-top: 70px;
            padding-bottom: 10px;
            background-color: white;
            font-family: "Lato", sans-serif;
        }
        .page_title{
            position: relative;
            width:95%;
            margin-left: auto;
            margin-right: auto; 
            font-size: 18px;
            font-weight: 600;
        }
        .links_container {
            position: relative;
            display: grid;
            grid-template-columns: auto auto auto auto;
            grid-row-gap:  10px; 
            width: 100%;
            height: auto;
            background-color: white;

        }

        @media (max-width:1000px) {
            .links_container {
                grid-template-columns: auto auto auto;
            }
        }

        @media (max-width:700px) {
            .links_container {
                grid-template-columns: auto;
            }
        }

        .single_link_container {
            position: relative;
            display: grid;
            justify-self: center;
            /*            float: left;*/
            width: 225px;
            grid-template-columns: auto;
            height: 135px;
            margin: 5px;
            transition: all 0.3s;
            border-radius: 8px;
            background-color: white;
            box-shadow: 0 0 13px rgba(0, 0, 0, 0.4);
            overflow: hidden;
        }

        .single_link_container a,
        .single_link_container button {
            outline: none !important;
            outline: 0 !important;
            border: 0px;
            text-decoration: none !important;
        }

        .single_link_container:hover {
            transform: scale(1.05);
        }

        .link_label {
            width: 100%;
            color: #202124;
            padding:0px 5px;
            font-family: "Lato", sans-serif;
            line-height: 20px;
            letter-spacing: 1px;
            text-shadow: 0 0 1px white;
            font-size: 22px;
            overflow:hidden;
            line-height: 1;
        }
        .item_link{
            position: absolute;
            top: 0%;
            left: 0%;
            width:100%;
            height: 100%;
            background: transparent;
        }

    </style>
</head>

<body>
    @include('header')
    <div class="page_container">
        <div class="page_title">
            <a href=""><i class="fas fa-home" style="font-size:20px;"></i> Home</a>
        </div>
        <div class="links_container">
            @if(isset($category))
            @foreach ($category as $cat)
            <div class="single_link_container" style="background-image:url('{{asset('images/'.$cat->category.'.jpg')}}');background-size: cover;background-position: center;">         
                <button class="link_label" style="background-color: rgba(0,0,0,0.3);color:white;">{{$cat->category}}
                    @if($cat->sub_category == "Y")
                    <a class="item_link" href="{{route('order.submenu',$cat->id)}}"></a></button>
                    @else
                    <a class="item_link" href="{{route('order.item_list',[$cat->id,'N'])}}"></a></button>
                    @endif
            </div>
           @endforeach
           @else
            @foreach ($sub_category as $sub_cat)
            <div class="single_link_container" style="background-image:url('{{asset('images/'.$sub_cat->sub_category.'.jpg')}}');background-size: cover;background-position: center;">         
                <button class="link_label" style="background-color: rgba(0,0,0,0.3);color:white;">{{$sub_cat->sub_category}}<a class="item_link" href="{{route('order.item_list',[$sub_cat->id,'Y'])}}"></a></button>
            </div>
            @endforeach
           @endif
        </div>
    </div>
</body>

</html>
