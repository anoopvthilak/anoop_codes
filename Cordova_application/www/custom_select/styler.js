function custom_select_box(dom_col, theme_no) {
    $(dom_col).each(function () {
        if ($(this).hasClass("custom_select_box_container")) {
            $(this).remove();
        }
    });
    var dom_col = $(dom_col);
    var theme_no;
    if(theme_no) {
        theme_no = theme_no;
    }
    else{
        theme_no = 1;
    }
    var theme_class="";
    if(typeof theme_no == "object"){
        for(i=0;i<theme_no.length;i++)
            theme_class += " select" + theme_no[i];
    }else{
        theme_class = "select" + theme_no;
    }

    dom_col.each(function () {
        var dom=$(this);
        dom.css("display", "none");
        var options = [],
            select_box_state = "hidden";
            
        dom.find("option").each(function () {
            options.push({
                value: $(this).attr("value"),
                label: $(this).html()
            });
        });
        //    console.log(options);
        var custom_select_box_container = $("<div />").attr("tabindex", "0").addClass(dom.attr("class") + " custom_select_box_container " + theme_class);
        custom_select_box_container.append("<div class='current_selection'><span>Select</span><div class='arrow'><i class='fas fa-angle-down'></i></div></div>");
        //    custom_select_box_container.append("<div class='current_selection'><span>Select</span><div style='text-align:center;'><i class='fas fa-chevron-up'></i></div></div>");
        custom_select_box_container.find(".current_selection span").html(dom.find(":selected").html()).attr("value", dom.val());
        var custom_select_box = $("<div />").addClass("custom_select_box").appendTo(custom_select_box_container);
        $.each(options, function (key, obj) {
            //        console.log(obj.label);
            $("<div />").addClass("custom_option").html(obj.label).attr("value", obj.value).appendTo(custom_select_box);
        });
        dom.after(custom_select_box_container);
        custom_select_box_container.on("click", ".custom_option", function () {
            if($(this).attr("value")==custom_select_box_container.find(".current_selection span").attr("value"))
                return;
            custom_select_box_container.find(".current_selection span").html($(this).html()).attr("value", $(this).attr("value"));
            dom.val($(this).attr("value"));
            dom.trigger("change");
        });
        custom_select_box_container.on("click", function () {
            if (select_box_state == "hidden") {
                open();
            } else {
                close();
            }
        });
        custom_select_box_container.on("focusout", function () {
            close();
        });
        var close_time_out;

        function open() {
            var offset = custom_select_box_container.offset();
            var parent_height = custom_select_box_container.outerHeight();
            var top = offset.top + parent_height - $(window).scrollTop();
            clearTimeout(close_time_out);
            custom_select_box_container.find(".arrow").css("transform", "rotate(-180deg)");
            custom_select_box_container.find(".custom_select_box").css({
                top: top,
                left: offset.left,
                "visibility": "visible",
                "opacity": "1",
                "width": custom_select_box_container.outerWidth()
            });
            select_box_state = "visible";
        }

        function close() {
            custom_select_box_container.find(".arrow").css("transform", "rotate(0deg)");
            custom_select_box_container.find(".custom_select_box").css("opacity", "0");
            select_box_state = "hidden";
            close_time_out = setTimeout(function () {
                custom_select_box_container.find(".custom_select_box").css("visibility", "hidden");
            }, 300);
        }
        dom.on("invalid",function(){
            custom_select_box_container.focus();
        });

        dom.parents("form").on("reset",function(){
            setTimeout(function(){
            custom_select_box_container.find(".current_selection span").html(dom.find(":selected").html()).attr("value", dom.val());
            },100);
        });

        $(document).on("scroll", function () {
            var offset = custom_select_box_container.offset();
            var parent_height = custom_select_box_container.outerHeight();
            var top = offset.top + parent_height - $(window).scrollTop();
            custom_select_box_container.find(".custom_select_box").css({
                top: top,
                left: offset.left
            });
            close();
        });
    });
}
